% compare the simulationresults of markus with mine 
% to find errors

load('../../MarkusSimulationresults.mat');

f = figure('Name','SensorData','NumberTitle','off');
figure(f);

% check if timeinterval is the same
if length(kiteTruthMarkus.time_s) ~= length(kiteTruth.time_s) || sum(kiteTruthMarkus.time_s'-kiteTruth.time_s) > 0.00001
    error('Timeinterval is not the same');
end

% filter values used:
filter_used = gamma_i_ahrsEulerDifferentSampleTime;

subplot(4,1,1);
title('Gyro');
hold('on');
plot(kiteTruth.time_s, sensorData.w_b(1,:),'r', 'DisplayName', 'w_roll');
plot(kiteTruth.time_s, sensorData.w_b(2,:),'g', 'DisplayName', 'w_pitch');
plot(kiteTruth.time_s, sensorData.w_b(3,:),'b', 'DisplayName', 'w_yaw');
plot(kiteTruth.time_s, kiteSensorsMarkus.gyro_wb_rps(:,1)','--m', 'DisplayName', 'w_rollMarkus');
plot(kiteTruth.time_s, kiteSensorsMarkus.gyro_wb_rps(:,2)','--y', 'DisplayName', 'w_pitchMarkus');
plot(kiteTruth.time_s, kiteSensorsMarkus.gyro_wb_rps(:,3)','--c', 'DisplayName', 'w_yawMarkus');
hold('off');

subplot(4,1,2);
title('Acceleration');
hold('on');
a_b_gmps2 = sensorData.a_b*g_mps2;
plot(kiteTruth.time_s, a_b_gmps2(1,:),'r', 'DisplayName', 'a_x');
plot(kiteTruth.time_s, a_b_gmps2(2,:),'g', 'DisplayName', 'a_y');
plot(kiteTruth.time_s, a_b_gmps2(3,:),'b', 'DisplayName', 'a_z');
plot(kiteTruth.time_s, kiteSensorsMarkus.accel_fb_mps2(:,1)','--m', 'DisplayName', 'a_xMarkus');
plot(kiteTruth.time_s, kiteSensorsMarkus.accel_fb_mps2(:,2)','--y', 'DisplayName', 'a_yMarkus');
plot(kiteTruth.time_s, kiteSensorsMarkus.accel_fb_mps2(:,3)','--c', 'DisplayName', 'a_zMarkus');
hold('off');

subplot(4,1,3);
title('Magnetometer');
hold('on');
plot(kiteTruth.time_s, sensorData.m_b(1,:),'r', 'DisplayName', 'a_x');
plot(kiteTruth.time_s, sensorData.m_b(2,:),'g', 'DisplayName', 'a_y');
plot(kiteTruth.time_s, sensorData.m_b(3,:),'b', 'DisplayName', 'a_z');
plot(kiteTruth.time_s, kiteSensorsMarkus.mag_mb_1(:,1)','--m', 'DisplayName', 'a_xMarkus');
plot(kiteTruth.time_s, kiteSensorsMarkus.mag_mb_1(:,2)','--y', 'DisplayName', 'a_yMarkus');
plot(kiteTruth.time_s, kiteSensorsMarkus.mag_mb_1(:,3)','--c', 'DisplayName', 'a_zMarkus');
hold('off');

subplot(4,1,4);
title('Velocity');
hold('on');
plot(kiteTruth.time_s, sensorData.v(1,:),'r', 'DisplayName', 'a_x');
plot(kiteTruth.time_s, sensorData.v(2,:),'g', 'DisplayName', 'a_y');
plot(kiteTruth.time_s, sensorData.v(3,:),'b', 'DisplayName', 'a_z');
plot(kiteTruth.time_s, kiteSensorsMarkus.GPS_ve_mps(:,1)','--m', 'DisplayName', 'a_xMarkus');
plot(kiteTruth.time_s, kiteSensorsMarkus.GPS_ve_mps(:,2)','--y', 'DisplayName', 'a_yMarkus');
plot(kiteTruth.time_s, kiteSensorsMarkus.GPS_ve_mps(:,3)','--c', 'DisplayName', 'a_zMarkus');
hold('off');

f = figure('Name','Filterresults','NumberTitle','off');
figure(f);
subplot(3,1,1);
hold('on');
plot(kiteTruth.time_s, kiteTruth.eul_deg(1,:),'k', 'DisplayName', 'Truthphi');
plot(kiteTruth.time_s, filter_used(1,:),'r', 'DisplayName', 'phi');
plot(kiteTruth.time_s, kiteEstMarkus.eul_deg(:,1)','--m', 'DisplayName', 'phiMarkus');
hold('off');
rmsd_phi_mine = RMSD(kiteTruth.eul_deg(1,:), filter_used(1,:));
rmsd_phi_markus = RMSD(kiteTruth.eul_deg(1,:), kiteEstMarkus.eul_deg(:,1)');
subplot(3,1,2);
hold('on');
plot(kiteTruth.time_s, kiteTruth.eul_deg(2,:),':k', 'DisplayName', 'Truththeta');
plot(kiteTruth.time_s, filter_used(2,:),'g', 'DisplayName', 'theta');
plot(kiteTruth.time_s, kiteEstMarkus.eul_deg(:,2)','--y', 'DisplayName', 'thetaMarkus');
hold('off');
rmsd_theta_mine = RMSD(kiteTruth.eul_deg(2,:), filter_used(2,:));
rmsd_theta_markus = RMSD(kiteTruth.eul_deg(2,:), kiteEstMarkus.eul_deg(:,2)');
subplot(3,1,3);
hold('on');
plot(kiteTruth.time_s, kiteTruth.eul_deg(3,:),'--k', 'DisplayName', 'Truthpsi');
plot(kiteTruth.time_s, filter_used(3,:),'b', 'DisplayName', 'psi');
plot(kiteTruth.time_s, kiteEstMarkus.eul_deg(:,3)','--c', 'DisplayName', 'psiMarkus');
hold('off');
rmsd_psi_mine = RMSD(kiteTruth.eul_deg(3,:), filter_used(3,:));
rmsd_psi_markus = RMSD(kiteTruth.eul_deg(3,:), kiteEstMarkus.eul_deg(:,3)');