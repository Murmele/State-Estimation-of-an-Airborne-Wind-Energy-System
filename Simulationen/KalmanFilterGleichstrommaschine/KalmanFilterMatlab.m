%% Berechnung der Schätzwerte bei vorgegebenen Kalman Gain
% Muss mit Simulink übereinstimmen
% Grenzwertberechung in "GrenzwertberechnungDifferenzengleichung.wxmx"

K = [8.19e-3;-8.445e-4]; % Aus Simulink modell
y = 50;
u=370;
x_k = [zeros(1,2000); zeros(1,2000)];
x_k2 = [zeros(1,2000); zeros(1,2000)];
unit_matrix = [1,0;0,1];
u=375; 
for i=1:2000
    x_k1=x_k(:,i);
    x_ktemp = G*x_k1+B*u;
    x_k(:,i+1)=x_ktemp+K*(y-H*x_ktemp);
end

close('all');
figure(1);
plot(x_k(1,:));


figure(2);
plot(x_k(2,:));

