clc;


fA = 100; % Abtastfrequenz [Hz]
TA = 1/fA; % Abtastperiode [s]
TA_noise = 0; %TA/1000;

% Ideales modell =0, modell mit rauschen = 1
ifNoiseGenerated = 1;

% ob discretes oder kontinuierliches Modell verwendet werden soll
ifDiscreteSystem=0;

% ob der Kalman Bucy durch 1/s integratoren (=0) oder durch einen Euler
% integrator (=1) dargestellt wird 
ifDiscreteKalmanBucy=1;

plotSimulinkModelToPDF = 0;

%% Motordaten (aus Luenberger-Beobachter_und_Extended_Kalman-Filter_Ein_Vergleich)
R = 0.3; % Motorwiderstand [Ohm]
L = 3e-3; % Motorinduktivität [H]
J = 0.2; % RotorTrägheit [kg m^2]
Psi = 1.13; % Permanentmagnet Fluss [Vs]
c_M = 1; % Motorkonstante [-]
U_n = 400; % Nennspannung [V]
M_n = 100; % Nennmoment [Nm]

%% Motordaten real, welcher der Motor aufweist (damit die systemmatrizen nicht übereinstimmen)
% Werden im Block system verwendet
R_real = R;
L_real = L;
J_real = J;
Psi_real = Psi;
c_M_real = c_M;
% Um System diskret aufzubauen:
% Differentialgleichung musste in Differenzengleichung umgewandelt werden
G_real = [1-R_real/L_real*TA, -Psi_real*c_M_real/L_real*TA; Psi_real*c_M_real/J_real*TA, 1]; % siehe "BerechnungDifferenzengleichung.wxmx"
B_real = [1/L_real*TA; 0]; % Eingangsmatrix 
D_real = [0;-1/J_real]*TA;

%% Kontinuierliche Systemgleichungen
G_cont = [-R/L, -Psi*c_M/L; Psi*c_M/J, 0];
B_cont = [1/L; 0];
H_cont = [1,0];
D_cont = [0; -1/J];

%% Measuring Errors (Varianzen aus Luenberger-Beobachter_und_Extended_Kalman-Filter_Ein_Vergleich)
sigma_i = 10000; %sigma^2
sigma_w = 100; % sigma^2
C_V_n_cont = [ sigma_i,0; 0, sigma_w ]; % Covariance of V_n
C_V_n = C_V_n_cont*TA^2;
C_W_n = 10; % Covariance of W_n
C_W_n_1 = inv(C_W_n);

%% Kalman Filter Parameter (diskrete Zustandsraum Darstellung)
% x1 = I(t)
% x2 = w(t)
% x_dot = G*x+B*u
% y = H*x
G = G_cont*TA+[1,0;0,1]; % siehe "BerechnungDifferenzengleichung.wxmx" [1-R/L*TA, -Psi*c_M/L*TA; Psi*c_M/J*TA, 1]
%D = [1/L; -1/J];
H = H_cont; % y(t) = I(t) = x1(t)
B = B_cont*TA; % Eingangsmatrix [1/L*TA; 0]
x0 = [0;0]; % initial value for x
K0 = C_V_n*transpose(H)*inv(C_W_n+H*C_V_n*transpose(H));
C_nn0 = C_V_n - K0*H*C_V_n; %[0,0;0,0]; % initial value for the covariance matrix

%% Kalman Filter Parameter (diskrete Zustandsraum Darstellung mit VanLoan Methode)
% entwickeln der diskreten Zustandsraumdarstellung aus der kontinuierlichen
% Zustandsraumdarstellung mit der VanLoan Methode
nStates = 2;
AA = [-G_cont, C_V_n_cont; zeros(nStates,nStates), G_cont']*TA;
BB = expm(AA);
PHI_k = BB(nStates+1:2*nStates, nStates+1: 2*nStates)';
Q_k = PHI_k*BB(1:nStates,nStates+1:2*nStates);
R_k = C_W_n/TA;
H_k = H_cont; % da keine differentialgleichung

if plotSimulinkModelToPDF
    saveas(get_param('KalmanFilterGleichstrommaschineBlockDiagramBilder','Handle'),'KalmanFilterGleichstrommaschine.pdf')
end

endTime = 1.5; % [s]
endIndex = find(result_i.Time > endTime, 1);
close all;
figure('Name', 'Current/Angular velocity');
ax(1) = subplot(2, 1, 1);
title('Current I [A]');
hold on;
plot(result_i.Time(1:endIndex), result_i.Data(1:endIndex, 1), 'DisplayName', 'Kalman-Bucy');
plot(result_i.Time(1:endIndex), result_i.Data(1:endIndex, 2), 'DisplayName', 'System');
plot(result_i.Time(1:endIndex), result_i.Data(1:endIndex, 3), 'DisplayName', 'Discrete Kalman filter');
plot(result_i.Time(1:endIndex), result_i.Data(1:endIndex, 4), 'DisplayName', 'System without noise');
hold off;
ax(2) = subplot(2, 1, 2);
title('Angular velocity \Omega [rad/s]');
hold on;
plot(results_omega.Time(1:endIndex), results_omega.Data(1:endIndex, 1), 'DisplayName', 'Kalman-Bucy');
plot(results_omega.Time(1:endIndex), results_omega.Data(1:endIndex, 2), 'DisplayName', 'System');
plot(results_omega.Time(1:endIndex), results_omega.Data(1:endIndex, 3), 'DisplayName', 'Discrete Kalman filter');
plot(results_omega.Time(1:endIndex), results_omega.Data(1:endIndex, 4), 'DisplayName', 'System without noise');
hold off;
linkaxes(ax,'x');
