% Vergleich von einem kontinuierlichem Modell mit verschiedenen diskreten
% Modellen, welche durch unterschiedliche Methoden diskretisiert wurden.
clc; clear;
%% Simulationseinstellungen
fA = 1000; % Abtastfrequenz [Hz]
TA = 1/fA; % Abtastperiode [s]
TA_noise = 0; %TA/1000;

% Ideales modell =0, modell mit rauschen = 1
ifNoiseGenerated = 1;

% ob discretes oder kontinuierliches Modell verwendet werden soll
ifDiscreteSystem=0;


%% Motordaten (aus Luenberger-Beobachter_und_Extended_Kalman-Filter_Ein_Vergleich)
R = 0.3; % Motorwiderstand [Ohm]
L = 3e-3; % Motorinduktivität [H]
J = 0.2; % RotorTrägheit [kg m^2]
Psi = 1.13; % Permanentmagnet Fluss [Vs]
c_M = 1; % Motorkonstante [-]
U_n = 400; % Nennspannung [V]
M_n = 100; % Nennmoment [Nm]

%% Measuring Errors (Varianzen aus Luenberger-Beobachter_und_Extended_Kalman-Filter_Ein_Vergleich)
sigma_i = 10000; %sigma^2
sigma_w = 100; % sigma^2
C_V_n_cont = [ sigma_i,0; 0, sigma_w ]; % Covariance of V_n
C_V_n = C_V_n_cont*TA^2;
C_W_n = 10; % Covariance of W_n

%% Kontinuierliche Systemgleichungen
G_cont = [-R/L, -Psi*c_M/L; Psi*c_M/J, 0];
B_cont = [1/L; 0];
D_cont = [0; -1/J];
H_cont = [1,0];

%% Diskretisierung 1.Variante (einfachste):
% Näherung x_dot ist ungefähr (x_k+1-x_k)/TA
G = G_cont*TA+[1,0;0,1];
H = H_cont; % y(t) = I(t) = x1(t)
B = B_cont*TA; % Eingangsmatrix [1/L*TA; 0]
D = D_cont*TA;

%% Diskretisierung 2.Variante (C.F. van Loan Methode)
% Introduction_to_Random_Signals_and_Applied_Kalman_Filtering-4th_edition
% (S. 140)
state_counts = 2; % i, omega sind die Zustände
A = zeros(2*state_counts,2*state_counts);
A(1:length(G_cont),1:length(G_cont)) = -G_cont;
A(length(G_cont)+1:2*length(G_cont),length(G_cont)+1:2*length(G_cont)) = G';
W = 1;
A(1:length(G_cont),length(G_cont)+1:2*length(G_cont)) = B_cont*W*B_cont';
A= A*TA;
B = expm(A); %compute B=e^A
phi = B(length(G_cont)+1:2*length(G_cont),length(G_cont)+1:2*length(G_cont))';
Q_k = phi*B(1:length(G_cont),length(G_cont)+1:2*length(G_cont));
