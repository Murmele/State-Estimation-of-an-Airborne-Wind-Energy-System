clc; clear;


fA = 1000; % Abtastfrequenz [Hz]
TA = 1/fA; % Abtastperiode [s]
TA_noise = 0; %TA/1000;

% Ideales modell =0, modell mit rauschen = 1
ifNoiseGenerated = 1;

% ob discretes oder kontinuierliches Modell verwendet werden soll
ifDiscreteSystem=0;

% ob der Kalman Bucy durch 1/s integratoren (=0) oder durch einen Euler
% integrator (=1) dargestellt wird 
ifDiscreteKalmanBucy=0;

%% Motordaten (aus Luenberger-Beobachter_und_Extended_Kalman-Filter_Ein_Vergleich)
R = 0.3; % Motorwiderstand [Ohm]
L = 3e-3; % Motorinduktivität [H]
J = 0.2; % RotorTrägheit [kg m^2]
Psi = 1.13; % Permanentmagnet Fluss [Vs]
c_M = 1; % Motorkonstante [-]
U_n = 400; % Nennspannung [V]
M_n = 100; % Nennmoment [Nm]

%% Motordaten real, welcher der Motor aufweist (damit die systemmatrizen nicht übereinstimmen)
% Werden im Block system verwendet
R_real = R;
L_real = L;
J_real = J;
Psi_real = Psi;
c_M_real = c_M;
% Um System diskret aufzubauen:
% Differentialgleichung musste in Differenzengleichung umgewandelt werden
G_real = [1-R_real/L_real*TA, -Psi_real*c_M_real/L_real*TA; Psi_real*c_M_real/J_real*TA, 1]; % siehe "BerechnungDifferenzengleichung.wxmx"
B_real = [1/L_real*TA; 0]; % Eingangsmatrix 

%% Kontinuierliche Systemgleichungen
G_cont = [-R/L, -Psi*c_M/L; Psi*c_M/J, 0];
B_cont = [1/L; 0];
H_cont = [1,0];

%% Measuring Errors (Varianzen aus Luenberger-Beobachter_und_Extended_Kalman-Filter_Ein_Vergleich)
sigma_i = 10000; %sigma^2
sigma_w = 100; % sigma^2
C_V_n_cont = [ sigma_i,0; 0, sigma_w ]; % Covariance of V_n
C_V_n = C_V_n_cont*TA^2;
C_W_n = 10; % Covariance of W_n
C_W_n_1 = inv(C_W_n);

%% Kalman Filter Parameter (diskrete Zustandsraum Darstellung)
% x1 = I(t)
% x2 = w(t)
% x_dot = G*x+B*u
% y = H*x
G = G_cont*TA+[1,0;0,1]; % siehe "BerechnungDifferenzengleichung.wxmx" [1-R/L*TA, -Psi*c_M/L*TA; Psi*c_M/J*TA, 1]
%D = [1/L; -1/J];
H = H_cont; % y(t) = I(t) = x1(t)
B = B_cont*TA; % Eingangsmatrix [1/L*TA; 0]
x0 = [0;0]; % initial value for x
K0 = C_V_n*transpose(H)*inv(C_W_n+H*C_V_n*transpose(H));
C_nn0 = C_V_n - K0*H*C_V_n; %[0,0;0,0]; % initial value for the covariance matrix