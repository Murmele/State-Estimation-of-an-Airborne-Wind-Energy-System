\subsection{Kalman filter}
In this section a brief derivation of the Kalman filter is presented, because it is one of the main filters used in this thesis. After the derivation a comparsion between the time-discrete Kalman filter and the discretized Kalman-Bucy filter is made.
\subsubsection{Linear Kalman Filter}
\label{sec:KalmanFilter}
The (linear) Kalman filter is a recursive filter which is based on estimating unknowns states of a linear system \begin{equation}
\label{eq:x_n}
\boldsymbol{X}_n = \boldsymbol{G}_n \boldsymbol{X}_{n-1} + \boldsymbol{B}_n\boldsymbol{u}_n+ \boldsymbol{V}_n
\end{equation}
\begin{equation}
\label{eq:y_n}
\boldsymbol{Y}_n = \boldsymbol{H}_n \boldsymbol{X}_n + \boldsymbol{W}_n
\end{equation}
with $\boldsymbol{V}_n$ the process noise and $\boldsymbol{W}_n$ the measurement noise\cite{Utschick2017,Reid2001} by minimizing the \ac{MSE} with a prior information about the noise. \\
In general the mean of a random variable $X$ is defined as
\begin{equation}
\label{eq:meanX}
E[X] = \int_{\mathbb{X}}x f_X(x)dx
\end{equation}
with $f_X(x)$ the \ac{PDF} of the random variable $X$. The variance of a random variable $X$ is defined as
\begin{equation}
\label{eq: }
VAR[X] = E[(X-E[X])^2]
\end{equation}
which can be extended for a multivariante random variable $\boldsymbol{X}$ to
\begin{equation}
\label{eq: }
C_{\boldsymbol{X} }= C[\boldsymbol{X}, \boldsymbol{X}] = E\left[(\boldsymbol{X}-E\left[\boldsymbol{X} \right])(\boldsymbol{X}-E\left[\boldsymbol{X} \right] )^T  \right] \text{.}
\end{equation}
The \ac{MSE} of an estimator $T(X)$ is defined as
\begin{equation}
\label{eq:MSE}
\epsilon [T(X)] = E\left[\left( T(X)-\Theta\right) ^2 \right]
\end{equation}
with $\Theta$ the real value, which should be estimated. The \ac{PDF} of the real value is defined as
\begin{equation}
\label{eq:pdf_theta}
f_\Theta\left( \theta; \sigma \right)
\end{equation}
with $\theta$ the mean and $\sigma$ the variance of the random variable $\Theta$.
The \ac{PDF} of the random variable $X$ with the knowledge of $\Theta$ is defined as
\begin{equation}
\label{eq:pdf_conditional_x_theta}
f_{X|\Theta}\left( x|\theta \right) \text{.}
\end{equation}
Minimizing the \ac{MSE} by using two times the definition of the mean \eqref{eq:meanX}
\begin{align}
\epsilon[T(X)] & = \int_\Theta \int_\mathbb{X} \left( T(x)-\theta \right) ^2 f_{X|\Theta}(x|\theta)\mathrm{d}x f_\Theta (\theta ; \sigma) \mathrm{d}\theta  \nonumber \\ 
&  = \int_\mathbb{X} \underbrace{\int_\Theta (T(x)-\theta)^2 f_{\Theta|X}(\theta|x)\mathrm{d}\theta}_{\text{minimize}} \underbrace{f_X(x)}_{\substack{\text{indipendent}\\ \text{of }\Theta}}\mathrm{d}x \nonumber \\
&\Rightarrow\min\limits_{T(x)} \int_\Theta (T(x)-\theta)^2 f_{\Theta|X}(\theta|x)\mathrm{d}\theta &  \nonumber \\ 
\label{eq:conditional_mean_estimator}
\end{align}
% im äußeren Integral ist kein Theta mehr vorhanden, weshalb dieser Teil beim Minimieren nichts bringt.
results in an estimator called conditional mean estimator or Bayes estimator\cite{Utschick2017,Geebelen2015}
\begin{equation}
\label{eq: }
T(x) = \int_\Theta \theta f_{\Theta | X }(\theta | x) \mathrm{d}\theta = E[\Theta | X = x]\text{.}
\end{equation}
This estimator minimizes the \ac{MSE}. The Kalman filter estimates random variables in a recursive manner using this estimator.\\
% Genauere Herleitung siehe Statistical Signal Processing Vorlesung
The Kalman filter is usable at the following assumptions
\begin{assumption}
	\label{as:XV_stock_independent}
	$\boldsymbol{X}_m$ and $\boldsymbol{V}_n$ are stochastically independent for any $m$,$n$:
	\begin{equation*}
	\label{eq: }
	E\left[\boldsymbol{X}_m \boldsymbol{V}_n \right] = E\left[\boldsymbol{X}_m \right] E\left[\boldsymbol{V}_n \right]
	\end{equation*}
\end{assumption}
\begin{assumption}
	\label{as:white_gaussian_noise}
	The process noise $\boldsymbol{V}_n$ and the measurement noise $\boldsymbol{W}_n$ are gaussian white noises with zero mean
	\begin{align}
		\boldsymbol{V}_n \sim \mathcal{N}\left( \boldsymbol{0} , \boldsymbol{C}_{\boldsymbol{V}_n}\right) \nonumber \\
		\boldsymbol{W}_n \sim \mathcal{N}\left( \boldsymbol{0} , \boldsymbol{C}_{\boldsymbol{W}_n}\right) \nonumber
	\end{align}
\end{assumption}
\begin{assumption}
	\label{as:VV_stoch_independent }
	The random variables $\boldsymbol{V}_k$,$\boldsymbol{V}_l$ and $\boldsymbol{W}_m$,$\boldsymbol{W}_n$ are stochastically independent for any $k$,$l$ and $m$, $n$:
	\begin{align}
			E\left[\boldsymbol{V}_k \boldsymbol{V}_l\right] = E\left[\boldsymbol{V}_k \right] E\left[\boldsymbol{V}_l \right] \nonumber \\
			E\left[\boldsymbol{W}_m \boldsymbol{W}_n\right] = E\left[\boldsymbol{W}_m \right] E\left[\boldsymbol{W}_n \right] \nonumber
	\end{align}
\end{assumption}
\begin{assumption}
	\label{as:VW_stoch_independent}
	The noises $\boldsymbol{V}_n$ and $\boldsymbol{W}_m$ are stochastically indipendent for any $n$ and $m$:
	\begin{align}
		E\left[\boldsymbol{V}_n \boldsymbol{W}_n\right] = E\left[\boldsymbol{V}_n \right] E\left[\boldsymbol{W}_n \right]  \nonumber
	\end{align}
\end{assumption}
The Kalman filter can be divided into two steps, the prediction step and the corrector step\cite{Utschick2017}. In the prediction step the expectation of the state $\boldsymbol{X}_n$ with the knowledge of $\boldsymbol{Y}_{(n-1)}$, which defines all observations until the step n-1, is defined as 
\begin{equation}
\label{eq:x_nn_1}
\hat{\boldsymbol{x}}_{n|n-1} = E\left[\boldsymbol{X}_n | \boldsymbol{Y}_{(n-1)}=\boldsymbol{y_{(n-1)}} \right] \text{.}
\end{equation}
The covariance of $\boldsymbol{X}_n$ is defined in \cite{Utschick2017} as
\begin{align}
C_{\boldsymbol{X}_{n|n-1}} = E\left[\left( \boldsymbol{X}_n - \hat{\boldsymbol{x}}_{n|n-1} \right) \left( \boldsymbol{X}_n - \hat{\boldsymbol{x}}_{n|n-1} \right)^T | \boldsymbol{Y}_{(n-1)}=\boldsymbol{y}_{(n-1)}\right] \text{.}
\label{eq:cx_nn_1}
\end{align}
$\hat{\boldsymbol{x}}_{n|n-1}$ can be derived from the Chapman-Kolmogorov equation \cite{Utschick2017} as
\begin{align}
	\label{eq:xnn1}
	\hat{\boldsymbol{x}}_{n|n-1} & = \underbrace{\int_{\mathbb{X}}\boldsymbol{x}_n f_{\boldsymbol{X}_n|\boldsymbol{Y}_{(n-1)}}(\boldsymbol{x}_n | \boldsymbol{y}_{(n-1)})\mathrm{d}\boldsymbol{x}_n}_{\text{estimator to minimize the \ac{MSE} \eqref{eq:conditional_mean_estimator}}}	\nonumber \\
	& = \int_{\mathbb{X}}\boldsymbol{x}_n \int_{\mathbb{X}}f_{\boldsymbol{X}_n | \boldsymbol{X}_{n-1}} (\boldsymbol{x}_n| \boldsymbol{x}_{n-1})\underbrace{f_{\boldsymbol{X}_{n-1}|\boldsymbol{Y}_{(n-1)}}(\boldsymbol{x}_{n-1}|\boldsymbol{y}_{(n-1)})}_{\text{indipendent from }\boldsymbol{x}_n}\mathrm{d}\boldsymbol{x}_{n-1}\mathrm{d}\boldsymbol{x}_n \nonumber \\
	&= \int_{\mathbb{X}}\underbrace{\int_{\mathbb{X}}\boldsymbol{x}_n f_{\boldsymbol{X}_n | \boldsymbol{X}_{n-1}}(\boldsymbol{x}_n | \boldsymbol{x}_{n-1})\mathrm{d}\boldsymbol{x}_n}_{E\left[\boldsymbol{x}_n | \boldsymbol{x}_{n-1} \right] = \boldsymbol{G}_n \boldsymbol{x}_{n-1}+\boldsymbol{B}_n \boldsymbol{u}_n\eqref{eq:x_n} } f_{\boldsymbol{X}_{n-1}| \boldsymbol{Y}_{(n-1)}} (\boldsymbol{x}_{n-1}| \boldsymbol{y}_{(n-1)})\mathrm{d}\boldsymbol{x}_{n-1} \nonumber \\ &
	=\int_{\mathbb{X}}\boldsymbol{G}_n \boldsymbol{x}_{n-1}f_{\boldsymbol{X}_{n-1}| \boldsymbol{Y}_{(n-1)}} (\boldsymbol{x}_{n-1}| \boldsymbol{y}_{(n-1)}) \mathrm{d}\boldsymbol{x}_{n-1} +\boldsymbol{B}_n \boldsymbol{u_n} \underbrace{\int_{\mathbb{X}}f_{\boldsymbol{X}_{n-1}| \boldsymbol{Y}_{(n-1)}} (\boldsymbol{x}_{n-1}| \boldsymbol{y}_{(n-1)})\mathrm{d}\boldsymbol{x}_{n-1}}_{= 1}  \nonumber \\
	&= \boldsymbol{G}_n \hat{\boldsymbol{x}}_{n-1|n-1}+\boldsymbol{B}_n \boldsymbol{u_n}\text{.}
\end{align}
The conditional state covariance matrix also called error covariance \cite{Brown2012} of $\boldsymbol{X}_n$ can be derived as
\begin{align}
	\label{eq:C_xnn1}
	\boldsymbol{C}_{X_{n|n-1}} &= E\left[\left( \boldsymbol{X}_n - \hat{\boldsymbol{x}}_{n|n-1} \right) \left( \boldsymbol{X}_n - \hat{\boldsymbol{x}}_{n|n-1} \right)^T | \boldsymbol{Y}_{(n-1)}=\boldsymbol{y}_{(n-1)}\right] \nonumber \\
	&= E\left[\left( \boldsymbol{G}_n \boldsymbol{X}_{n-1} +\boldsymbol{B}_n \boldsymbol{u_n}+\boldsymbol{V}_n - (\boldsymbol{G}_n \hat{\boldsymbol{x}}_{n-1|n-1}+\boldsymbol{B}_n \boldsymbol{u_n}) \right)\right. \nonumber \\ &\left. \left( \boldsymbol{G}_n \boldsymbol{X}_{n-1} +\boldsymbol{V}_n - (\boldsymbol{G}_n \hat{\boldsymbol{x}}_{n-1|n-1}+\boldsymbol{B}_n \boldsymbol{u_n}) \right)^T| \boldsymbol{Y}_{(n-1)}=\boldsymbol{y}_{(n-1)}\right] \nonumber \\
	&= 
	E\left[ \boldsymbol{G}_n (\boldsymbol{X}_n - \hat{\boldsymbol{x}}_{n-1|n-1})(\boldsymbol{X}_n - \hat{\boldsymbol{x}}_{n-1|n-1})^T \boldsymbol{G}_n^T| \boldsymbol{Y}_{(n-1)}=\boldsymbol{y}_{(n-1)}v\right] \nonumber \\ 
	&- \underbrace{E\left[\boldsymbol{V}_n \hat{\boldsymbol{x}}_{n-1|n-1}^T \boldsymbol{G}_n^T| \boldsymbol{Y}_{(n-1)}=\boldsymbol{y}_{(n-1)}\right]}_{= E\left[\boldsymbol{V}_n \right] \hat{\boldsymbol{x}}_{n-1|n-1}^T \boldsymbol{G}_n^T \underset{\ref{as:white_gaussian_noise}}{=} 0} 
	- \underbrace{E\left[\boldsymbol{G}_n \hat{\boldsymbol{x}}_{n-1|n-1}\boldsymbol{V}_n^T| \boldsymbol{Y}_{(n-1)}=\boldsymbol{y}_{(n-1)}\right]}_{= \boldsymbol{G}_n \hat{\boldsymbol{x}}_{n-1|n-1} E\left[\boldsymbol{V}_n^T| \boldsymbol{Y}_{(n-1)}=\boldsymbol{y}_{(n-1)} \right] \underset{\ref{as:white_gaussian_noise}}{=} 0} \nonumber \\&
	+\underbrace{ E\left[\boldsymbol{G}_n \boldsymbol{X}_n \boldsymbol{V}_n^T| \boldsymbol{Y}_{(n-1)}=\boldsymbol{y}_{(n-1)}\right]}_{\underset{\ref{as:XV_stock_independent}}{=}\boldsymbol{G}_n E\left[ \boldsymbol{X}_n \right] E\left[ \boldsymbol{V}_n^T | \boldsymbol{Y}_{(n-1)}=\boldsymbol{y}_{(n-1)}\right] \underset{\ref{as:white_gaussian_noise}}{=} 0} 
	+ \underbrace{E\left[\boldsymbol{V}_n \boldsymbol{X}_n^T \boldsymbol{G}_n^T| \boldsymbol{Y}_{(n-1)}=\boldsymbol{y}_{(n-1)}\right]}_{\underset{\ref{as:XV_stock_independent}}{=}E\left[ \boldsymbol{V}_n E\left[ \boldsymbol{X}_n^T| \boldsymbol{Y}_{(n-1)}=\boldsymbol{y}_{(n-1)}\right]\boldsymbol{G}_n^T  \right] \underset{\ref{as:white_gaussian_noise}}{=} 0}\\
	&+ E\left[\boldsymbol{V}_n \boldsymbol{V}_n^T | \boldsymbol{Y}_{(n-1)}=\boldsymbol{y}_{(n-1)}\right] 	\nonumber \\
	&= \boldsymbol{G}_n \boldsymbol{C}_{\boldsymbol{X}_{n-1|n-1}}\boldsymbol{G}_n^T + \boldsymbol{C}_{\boldsymbol{V}_n}\text{.}
\end{align}
In the corrector step the Mean of $\boldsymbol{X}_n$ respect to $\boldsymbol{Y}_{(n)}$ will be calculated with the Bayes rule\cite{Utschick2017}
\begin{equation}
\label{eq:bayes_rule}
f_{\boldsymbol{X}_n|\boldsymbol{Y}_{(n)}} (\boldsymbol{x}_n | \boldsymbol{y}_{n}) = \frac{f_{\boldsymbol{Y}_n | \boldsymbol{X}_n}(\boldsymbol{y}_n | \boldsymbol{x}_n)f_{\boldsymbol{X}_n | \boldsymbol{Y}_{(n-1)}}(\boldsymbol{x}_n | \boldsymbol{y}_{n-1})}{f_{\boldsymbol{Y}_n | \boldsymbol{Y}_{(n-1)}}(\boldsymbol{y}_n | \boldsymbol{y}_{(n-1)})}\text{.}
\end{equation}
With the assumption \ref{as:white_gaussian_noise} all probability density functions of \eqref{eq:bayes_rule} are gaussian. By comparing the exponents of \eqref{eq:bayes_rule} we get the equation
%\begin{align}
%\label{eq:comparing_exponents}
%-\frac{1}{2}\left( x_n - \hat{x}_{n|n} \right)^T \boldsymbol{C}_{\boldsymbol{X}_{n|n}}^{-1}\left( x_n - \hat{x}_{n|n} \right)  &= 
%-\frac{1}{2}\left( \boldsymbol{y}_n - \mu _{\boldsymbol{Y}_n|\boldsymbol{X}_{n}} \right) ^T \boldsymbol{C}_{\boldsymbol{Y}_{n}|\boldsymbol{X}_n}^{-1} \left( \boldsymbol{y}_n - \mu _{\boldsymbol{Y}_n|\boldsymbol{X}_{n}} \right) \nonumber \\ 
% & -\frac{1}{2}\left( x_n - \hat{x}_{n|n-1} \right) ^T \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}^{-1} \left( x_n - \hat{x}_{n|n-1} \right) \nonumber \\
% &+ \frac{1}{2}\left( \boldsymbol{y}_n - \mu _{\boldsymbol{Y}_n|\boldsymbol{Y}_{(n-1)}} \right) ^T \boldsymbol{C}_{\boldsymbol{Y}_{n}|\boldsymbol{Y}_{(n-1)}}^{-1} \left( \boldsymbol{y}_n - \mu _{\boldsymbol{Y}_n|\boldsymbol{Y}_{(n-1)}} \right) \\
% \Rightarrow x_n^T \boldsymbol{C}_{\boldsymbol{X}_{n|n}}^{-1}x_n &- 2x_n^T \boldsymbol{C}_{\boldsymbol{X}_{n|n}}^{-1} \hat{x}_{n|n} + \hat{x}_{n|n}^T \boldsymbol{C}_{\boldsymbol{X}_{n|n}}^{-1} \hat{x}_{n|n} = \nonumber\\ &
% \underbrace{\boldsymbol{y}_n^T}_{= x_n^T \boldsymbol{H}_n^T+W_n^T} \boldsymbol{C}_{\boldsymbol{Y}_{n}|\boldsymbol{X}_n}^{-1}\underbrace{\boldsymbol{y}_n}_{= \boldsymbol{H}_n x_n + W_n} - 2\boldsymbol{y}_n^T \boldsymbol{C}_{\boldsymbol{Y}_{n}|\boldsymbol{X}_n}^{-1} \mu _{\boldsymbol{Y}_n|\boldsymbol{X}_{n}} + \mu _{\boldsymbol{Y}_n|\boldsymbol{X}_{n}}^T \boldsymbol{C}_{\boldsymbol{Y}_{n}|\boldsymbol{X}_n}^{-1} \mu _{\boldsymbol{Y}_n|\boldsymbol{X}_{n}} \nonumber\\&
% +x_n^T \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}^{-1}x_n - 2x_n^T\boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}^{-1} \hat{x}_{n|n-1} + \hat{x}_{n|n-1}^T \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}^{-1} \hat{x}_{n|n-1} \nonumber\\&
%- \left( \boldsymbol{y}_n^T \boldsymbol{C}_{\boldsymbol{Y}_{n}|\boldsymbol{Y}_{(n-1)}}^{-1}\boldsymbol{y}_n - 2\boldsymbol{y}_n^T\boldsymbol{C}_{\boldsymbol{Y}_{n}|\boldsymbol{Y}_{(n-1)}}^{-1} \mu _{\boldsymbol{Y}_n|\boldsymbol{Y}_{(n-1)}} + \mu _{\boldsymbol{Y}_n|\boldsymbol{Y}_{(n-1)}}^T \boldsymbol{C}_{\boldsymbol{Y}_{n}|\boldsymbol{Y}_{(n-1)}}^{-1} \mu _{\boldsymbol{Y}_n|\boldsymbol{Y}_{(n-1)}} \right) 
%\end{align}
% mit propotionalität S. 123 statistical signal processing
\begin{align}
\label{eq:comparing_exponents}
-\frac{1}{2}\left( \boldsymbol{x}_n - \hat{\boldsymbol{x}}_{n|n} \right)^T \boldsymbol{C}_{\boldsymbol{X}_{n|n}}^{-1}\left( \boldsymbol{x}_n - \hat{\boldsymbol{x}}_{n|n} \right)  &\propto 
-\frac{1}{2}\left( \boldsymbol{y}_n - \mu _{\boldsymbol{Y}_n|\boldsymbol{X}_{n}} \right) ^T \boldsymbol{C}_{\boldsymbol{Y}_{n}|\boldsymbol{X}_n}^{-1} \left( \boldsymbol{y}_n - \mu _{\boldsymbol{Y}_n|\boldsymbol{X}_{n}} \right) \nonumber \\ 
& -\frac{1}{2}\left( \boldsymbol{x}_n - \hat{\boldsymbol{x}}_{n|n-1} \right) ^T \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}^{-1} \left( \boldsymbol{x}_n - \hat{\boldsymbol{x}}_{n|n-1} \right) \nonumber \\
\Rightarrow \boldsymbol{x}_n^T \boldsymbol{C}_{\boldsymbol{X}_{n|n}}^{-1}\boldsymbol{x}_n &- 2\boldsymbol{x}_n^T \boldsymbol{C}_{\boldsymbol{X}_{n|n}}^{-1} \hat{\boldsymbol{x}}_{n|n} +...\propto \nonumber\\ 
\boldsymbol{x}_n^T \underbrace{\left( \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}^{-1}+\boldsymbol{H}_n^T \boldsymbol{C}_{\boldsymbol{Y}_n|\boldsymbol{X}_n=\boldsymbol{x}_n}^{-1}\boldsymbol{H}_n \right)}_{\boldsymbol{C}_{\boldsymbol{X}_{n|n}}^{-1}}\boldsymbol{x}_n - & 2\boldsymbol{x}_n^T \underbrace{\left( \boldsymbol{H}_n^T \boldsymbol{C}_{\boldsymbol{Y}_n|\boldsymbol{X}_n=\boldsymbol{x}_n}^{-1}\boldsymbol{y}_n + \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}^{-1} \hat{\boldsymbol{x}}_{n|n-1} \right)}_{\boldsymbol{C}_{\boldsymbol{X}_{n|n}}^{-1} \hat{\boldsymbol{x}}_{n|n}}+...
\end{align} 
By comparing the terms of \eqref{eq:comparing_exponents} we get the covariance matrix
\begin{align}
\label{eq:C_xnn}
\boldsymbol{C}_{\boldsymbol{X}_{n|n}} &= \left( \boldsymbol{H}_n^T \boldsymbol{C}_{\boldsymbol{Y}_n|\boldsymbol{X}_n=\boldsymbol{x}_n}^{-1}\boldsymbol{y}_n + \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}^{-1} \hat{\boldsymbol{x}}_{n|n-1}  \right) ^{-1} \nonumber \\ & =  \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}} -\underbrace{ \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}\boldsymbol{H}_n^T\overbrace{\left( \underbrace{\boldsymbol{C}_{\boldsymbol{Y}_n | \boldsymbol{X}_n = \boldsymbol{x}_n}}_{\boldsymbol{C}_{\boldsymbol{W}_n}} + \boldsymbol{H}_n \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}\boldsymbol{H}_n^T \right) ^{-1}}^{\boldsymbol{C}_{\boldsymbol{Y}_{n|n-1}}^{-1}}}_{\text{Kalman Gain } \boldsymbol{K}_n} \boldsymbol{H}_n \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}} \nonumber \\
& = \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}} - \boldsymbol{K}_n \boldsymbol{H}_n \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}
\end{align}
with $\boldsymbol{K}_n$ the Kalman gain. Replacing of $\boldsymbol{C}_{\boldsymbol{X}_{n|n}}$ in $\hat{\boldsymbol{x}}_{n|n}$\eqref{eq:comparing_exponents} with \eqref{eq:C_xnn} we get
\begin{equation}
\label{eq:x_nn}
\hat{\boldsymbol{x}}_{n|n} = \hat{\boldsymbol{x}}_{n|n-1} + \boldsymbol{K}_n \left( \boldsymbol{y}_n - \boldsymbol{H}_n \hat{\boldsymbol{x}}_{n|n-1} \right)\text{.}
\end{equation}
With the equations \eqref{eq:xnn1}, \eqref{eq:C_xnn1}, \eqref{eq:C_xnn} and \eqref{eq:x_nn} we get a recursive filter which minimizes the \ac{MSE}. There are different other names for the process noise covariance matrix $\boldsymbol{C}_{V_n}$, often called $\boldsymbol{Q}$ matrix and the measurement noise covariance matrix $\boldsymbol{C}_{W_n}$ called $\boldsymbol{R}$ matrix.

\paragraph{Summary}
\FloatBarrier
\begin{table}
	\centering
	\begin{tabular}{ | c | l | l |}
		\hline
		1 & calculate $\hat{\boldsymbol{x}}_{n|n-1}$ with equation \eqref{eq:xnn1} & $\hat{\boldsymbol{x}}_{n|n-1} = \boldsymbol{G}_n \hat{\boldsymbol{x}}_{n-1|n-1}+\boldsymbol{B}_n \boldsymbol{u_n}$ \\
		\hline
		2 & calculate $\boldsymbol{C}_{X_{n|n-1}}$ with equation \eqref{eq:C_xnn1} & $ \boldsymbol{C}_{X_{n|n-1}} = \boldsymbol{G}_n \boldsymbol{C}_{\boldsymbol{X}_{n-1|n-1}}\boldsymbol{G}_n^T + \boldsymbol{C}_{\boldsymbol{V}_n} $ \\
		\hline
		3 & calculate Kalman gain $\boldsymbol{K}_n$ (\eqref{eq:C_xnn}) & $\boldsymbol{K}_n =  \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}\boldsymbol{H}_n^T\overbrace{\left( \underbrace{\boldsymbol{C}_{\boldsymbol{Y}_n | \boldsymbol{X}_n = \boldsymbol{x}_n}}_{\boldsymbol{C}_{\boldsymbol{W}_n}} + \boldsymbol{H}_n \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}\boldsymbol{H}_n^T \right) ^{-1}}^{\boldsymbol{C}_{\boldsymbol{Y}_{n|n-1}}^{-1}}$\\
		\hline
		4 & Calculate $\boldsymbol{C}_{\boldsymbol{X}_{n|n}}$ with \eqref{eq:C_xnn} & $\boldsymbol{C}_{\boldsymbol{X}_{n|n}} = \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}} - \boldsymbol{K}_n \boldsymbol{H}_n \boldsymbol{C}_{\boldsymbol{X}_{n|n-1}}$ \\
		\hline
		5 & Calculate $\hat{\boldsymbol{x}}_{n|n}$ with \eqref{eq:x_nn} & $\hat{\boldsymbol{x}}_{n|n} =  \hat{\boldsymbol{x}}_{n|n-1} + \boldsymbol{K}_n \left( \boldsymbol{y}_n - \boldsymbol{H}_n \hat{\boldsymbol{x}}_{n|n-1} \right)$ \\
		\hline
	\end{tabular}
	\caption{Steps of the Kalman filter.}
\end{table}
\FloatBarrier
\paragraph{Stationary accuracy}
% source: Siehe GrenzwertberechnungDifferenzengleichung.wxmx
To calculate the stationary accuracy it is possible to use equation \eqref{eq:x_nn} and \eqref{eq:x_nn_1} and set $x_{n|n}$ equal to $x_{n|n-1}$. As a result of this procedure we get the stationary accuracy as

\begin{equation}
\label{eq: }
x_{error} = (\boldsymbol{I}_x-\boldsymbol{G})^{-1} (\boldsymbol{B}\boldsymbol{u}+\boldsymbol{C}\boldsymbol{M}_L) - (\boldsymbol{I}_x-(\boldsymbol{G}-\boldsymbol{K}\boldsymbol{H}\boldsymbol{G}))^{-1} \boldsymbol{D}\text{.}
\end{equation}

\paragraph{Converting time-continuous system to time-discrete system}
%A system \eqref{eq:cont_system1} and \eqref{eq:cont_system2} can be transformed in the discrete form \eqref{eq:x_n } and \eqref{eq:y_n} with \eqref{eq:discrete_differentiation}. With $T_A$ the sample rate.
%
%\begin{equation}
%\label{eq:discrete_differentiation}
%\dot{\boldsymbol{Y}} = \underset{T_A \rightarrow 0}{lim}\frac{\boldsymbol{Y}_k-\boldsymbol{Y}_{k-1}}{T_A}
%\end{equation}
%
%\begin{align}
%\dot{\boldsymbol{X}}(t) = \boldsymbol{G}(t)\boldsymbol{X}(t)+\boldsymbol{B}(t)\boldsymbol{u}(t)+\boldsymbol{V} \label{eq:cont_system1} \\
%\boldsymbol{Y}(t) = \boldsymbol{H}(t)\boldsymbol{X}(t)+\boldsymbol{W} \label{eq:cont_system2}
%\end{align}
%The discrete system looks like \eqref{eq:x_n_disc } and\eqref{eq:y_n_disc}.
%\begin{equation}
%\label{eq:x_n_disc }
%\boldsymbol{X}_n = \left(\boldsymbol{G}_nT_A+\boldsymbol{I}\right) \boldsymbol{X}_{n-1} + \boldsymbol{B}_nT_A\boldsymbol{u}_n+ T_A\boldsymbol{V}_n
%\end{equation}
%
%\begin{equation}
%\label{eq:y_n_disc}
%\boldsymbol{Y}_n = \boldsymbol{H}_n \boldsymbol{X}_n + \boldsymbol{W}_n
%\end{equation}
%The covariance matrix $C_{V_n}$ must be multiplied with a scale factor:
%\begin{equation}
%\label{eq: }
%\boldsymbol{C}_{\boldsymbol{V}_n} = T_A^2 \boldsymbol{C}_{\boldsymbol{V}}
%\end{equation}

%A system \eqref{eq:cont_system1} and \eqref{eq:cont_system2} can be transformed in the discrete form \eqref{eq:x_n } and \eqref{eq:y_n} with \eqref{eq:discrete_differentiation}.

Systems are in general time-continuous, but sampling of measurements with sensors, discrete samples arise. Therefore, the time-continuous system must be transferred to a time-discrete representation. This can be done with the method developed by C. F. van Loan \cite{Brown2012}. Assuming a system
\begin{equation}
\label{eq:x_cont}
\dot{\boldsymbol{x}}(t) = \boldsymbol{G} \boldsymbol{x}(t) + \boldsymbol{V} \boldsymbol{v}(t)
\end{equation}
and
\begin{equation}
\label{eq:y_cont}
\boldsymbol{y}(t) = \boldsymbol{H} \boldsymbol{x}(t) + \boldsymbol{w}
\end{equation}
which can be transfered into a discrete representation as shown in \eqref{eq:x_n} and \eqref{eq:y_n} with
\begin{equation}
\label{eq: }
\tilde{\boldsymbol{G}} = \begin{bmatrix}
-\boldsymbol{G} & \boldsymbol{V}\boldsymbol{W}\boldsymbol{V}^T \\
\boldsymbol{0} & \boldsymbol{G}^T
\end{bmatrix} \Delta t
\end{equation}
where $\boldsymbol{W}$ is the power spectral density of $\boldsymbol{v(t)}$. If $\boldsymbol{v(t)}$ is white noise the spectral density becomes a unit matrix.
\begin{equation}
\label{eq: }
\boldsymbol{G_{exp}} = expm(\tilde{\boldsymbol{G}}) = \begin{bmatrix}
\cdots & \boldsymbol{G}_n ^{-1} \boldsymbol{C}_{Vn} \\
0 & \boldsymbol{G}_n ^T
\end{bmatrix}
\end{equation}
with $expm$ the matrix exponential.
\begin{equation}
\label{eq: }
\boldsymbol{H}_n = \boldsymbol{H}
\end{equation}
\begin{equation}
\label{eq: }
\boldsymbol{C}_{Wn} = \frac{VAR[w]}{\Delta t}\text{.}
\end{equation}
The transition matrix $\boldsymbol{G}_n$ can be determined by transposing the lower right part of the matrix $\boldsymbol{G_{exp}}$, and the process noise matrix $\boldsymbol{V}_n$ from multiplying the upper right part of $\boldsymbol{G_{exp}}$ with the transition matrix.\\
% no need to calculate B_k directly, because 
% x_n|n-1 is calculated directly by x_n|n-1 = x_n-1|n-1 + x_dot * TA
% the transition matrix is needed to calculate the covariance.
\paragraph{Comparing discrete Kalman filter with Kalman-Bucy Filter}
The original implementation of the Kalman filter was made with the assumption, of a discrete system. In some cases a time-continous model is present and so the Kalman filter was extended to time-continous models. This extension is named Kalman-Bucy Filter \cite{Murray2010}. In this section a comparsion between the time-discrete and the time-continuous model is made for a DC-motor because it is much simpler than an attitude estimator which must be linearized due to the nonlinearity of the rotation matrices. The block diagram for the comparsion are visible in \ref{fig:kalmanBucySimulink}, \ref{fig:kalmanBucySimulinkKalmanBucy} and \ref{fig:kalmanBucySimulinkKalman}. The discrete Kalman filter is driven from a function call with a frequency of \SI{1000}{\hertz}. The system of the DC-motor is described as
\begin{equation}
\label{eq: }
\dot{\boldsymbol{x}}(t) = \begin{bmatrix}
\dot{i}(t) \\ \dot{\omega}(t)
\end{bmatrix} = \begin{bmatrix}
-\frac{R}{L} & -\frac{c_M \Psi_n}{L} \\ \frac{c_M \Psi_n}{J} & 0
\end{bmatrix}\begin{bmatrix}
i(t) \\ \omega (t)
\end{bmatrix} + \begin{bmatrix}
\frac{1}{L} \\ 0
\end{bmatrix}u(t) + \begin{bmatrix}
0 \\ -\frac{1}{J}
\end{bmatrix}M_L(t) + \begin{bmatrix}
\eta_ {\dot{i}} \\ \eta_{\dot{\omega}}
\end{bmatrix}
\end{equation}%
and
\begin{equation}
\label{eq: }
y(t) = \begin{bmatrix}
1 & 0
\end{bmatrix} \begin{bmatrix}
i(t) \\ \omega (t)
\end{bmatrix} + \eta_i = i(t) + \eta_i
\end{equation}
with the wire resistance $R$, the inductance $L$, the magnetic flux $\Psi_n$ and the motor constant $c_M$\cite{Bauer2013}. Gaussian noises $\eta_ {\dot{i}}$, $\eta_{\dot{\omega}}$ and $\eta_i$ are added to the ideal system. The values of these constants and the covariance matrix entries are used from F. Bauer\cite{Bauer2013} as
\begin{equation}
\label{eq: }
R = \SI{0,3}{\Omega}
\end{equation}
\begin{equation}
\label{eq: }
L = \SI{3}{\milli \henry}
\end{equation}
\begin{equation}
\label{eq: }
c_M\Psi_n = \SI{1,13}{\volt\second}
\end{equation}
\begin{equation}
\label{eq: }
J = \SI{0,2}{\kilogram \square\meter}
\end{equation}
\begin{equation}
\label{eq: }
\boldsymbol{C}_V = \begin{bmatrix}
VAR[\eta_{\dot{i}}] & 0 \\ 0 & VAR[\eta_{\dot{\omega}}]
\end{bmatrix} = \begin{bmatrix}
10000 & 0 \\ 0 & 100
\end{bmatrix}
\end{equation}
\begin{equation}
\label{eq: }
C_W = VAR[\eta_i] = 10
\end{equation}
\begin{figure}
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterGleichstrommaschine/KalmanFilterGleichstrommaschine}
	\caption{Comparsion of a discrete Kalman filter with a time-continous Kalman-Bucy filter.}
	\label{fig:kalmanBucySimulink}
\end{figure}
\begin{figure}[ht]
	\includegraphics[width=0.9\columnwidth ]{../Simulationen/KalmanFilterGleichstrommaschine/KalmanFilterGleichstrommaschine_Kalman-BucyFilter}
	\caption{Blockdiagram of the Kalman-Bucy filter\cite{Bauer2013}.}
	\label{fig:kalmanBucySimulinkKalmanBucy}
\end{figure}
\begin{figure}[ht]
	\includegraphics[width=0.9\columnwidth ]{../Simulationen/KalmanFilterGleichstrommaschine/KalmanFilterGleichstrommaschine_DiskreterKalmanFilter}
	\caption{Blockdiagram of the discrete Kalman filter.}
	\label{fig:kalmanBucySimulinkKalman}
\end{figure}\\
The result can be seen in figure \ref{fig:kalmanBucySimulinkZoomOut} and \ref{fig:kalmanBucySimulinkZoomIn}. There is a difference between the time-continous Kalman-Bucy filter and the time-discrete Kalman filter for the current about \SI{5}{\ampere} and for the angular velocity about \SI{0.2}{\radian\per\second}, but the dynamics of the filters are the same.
\begin{figure}[ht]
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterGleichstrommaschine/ResultPlots/ZoomOut}
%	\input{../Simulationen/KalmanFilterGleichstrommaschine/ResultPlots/ZoomOut2.tex}
	\caption{Filter dynamics comparsion.}
	\label{fig:kalmanBucySimulinkZoomOut}
\end{figure}
\begin{figure}[ht]
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterGleichstrommaschine/ResultPlots/ZoomIn}
%	\input{../Simulationen/KalmanFilterGleichstrommaschine/ResultPlots/ZoomIn2.tex}
	\caption{Zoom in of figure \ref{fig:kalmanBucySimulinkZoomOut}.}
	\label{fig:kalmanBucySimulinkZoomIn}
\end{figure}\\
Discretizing the Kalman-Bucy filter by replacing in Simulink the continuous integrator with an Euler integrator and a sample frequency of \SI{100}{\hertz}, the result is visible in figure \ref{fig:kalmanBucySimulinkDiscrete}. In the stationary case the results are the same as for the discrete Kalman filter, but the dynamics are different. The benefit of the Kalman-Bucy filter is, no matrix inversion must be calculated which speeds up the filter velocity and increases the accuracy, because inverting matrices of small or big values lead to rounding errors\cite{M.Rump2019}. The drawback is, just one sample time can be used, but the sensors have different sample times which is described in section \ref{sec:DST}.
\begin{figure}[ht]
	\centering
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterGleichstrommaschine/ResultPlots/100Hz}
%	\input{../Simulationen/KalmanFilterGleichstrommaschine/ResultPlots/100Hz2.tex}
	\caption{Comparsion of the discretised Kalman filter with the discretised Kalman-Bucy filter at a sample frequency of \SI{100}{\hertz}.}
	\label{fig:kalmanBucySimulinkDiscrete}
\end{figure}
\FloatBarrier
\subsubsection{Extended Kalman Filter (EKF)}

The \ac{EKF} is a derivation from the linear Kalman filter to use the filter for nonlinear systems by linearizing the system. On considers a system
\begin{equation}
\label{eq:x_n_nonlinear}
\boldsymbol{X}_n = \boldsymbol{g}\left( \boldsymbol{X}_{n-1|n-1}, \boldsymbol{B}_n\right) + \boldsymbol{V}_n
\end{equation}

\begin{equation}
\label{eq:y_n_nonlinear}
\boldsymbol{Y}_n = \boldsymbol{h}\left(\boldsymbol{X}_n \right) + \boldsymbol{W}_n
\end{equation}
with the Taylor Series of $\boldsymbol{g}()$ about the point $x_{n|n-1}$\cite{Terejanu}
\begin{equation}
\label{eq:taylor_of_g}
\boldsymbol{g}\left( \boldsymbol{X}_{n-1|n-1}, \boldsymbol{B}_n\right) = \boldsymbol{g}\left( \boldsymbol{\hat{X}}_{n-1|n-1}, \boldsymbol{B}_n\right) + J_g(\hat{X}_{n-1|n-1}, \boldsymbol{B}_n) (x_{n-1|n-1} - \hat{x}_{n-1|n-1}) + H.O.T
\end{equation}
with H.O.T the neglected higher order terms\cite{Terejanu} and $J_g$ is the jacobian matrix defined as
\begin{equation}
\label{eq:JacobianMatrix}
J_g \coloneqq \begin{bmatrix}
\frac{\partial g_1}{\partial x_1} & \frac{\partial g_1}{\partial x_2} & \cdots & \frac{\partial g_1}{\partial x_n} \\
\vdots & & \ddots & \vdots \\
\frac{\partial g_n}{\partial x_1} & \frac{\partial g_n}{\partial x_2} & \cdots & \frac{\partial g_n}{\partial x_n}\text{.}
\end{bmatrix}
\end{equation}
The mean of equation \eqref{eq:x_n_nonlinear} with respect to all measurements until $\boldsymbol{y}_{(n-1)}$ can be calculated with \eqref{eq:taylor_of_g}

\begin{equation}
\label{eq: }
\hat{\boldsymbol{x}}_{n|n-1} = E[\boldsymbol{X}_n | \boldsymbol{Y}_{(n-1)} = \boldsymbol{y}_{(n-1)}] = E[\boldsymbol{g}\left( \boldsymbol{X}_{n-1|n-1}, \boldsymbol{B}_n\right) + \boldsymbol{V}_n]  = 
\boldsymbol{g}\left( \boldsymbol{\hat{X}}_{n-1|n-1}, \boldsymbol{B}_n\right)\text{.}
\end{equation}
The covariance matrix $\boldsymbol{C}_{(n|n-1)}$ is calculated as
\begin{equation}
\label{eq:Cxnn1EKF}
\boldsymbol{C}_{\boldsymbol{X}_{(n|n-1)}} = J_g(\boldsymbol{\hat{X}}_{n-1|n-1}, \boldsymbol{B}_n) + \boldsymbol{C}_{\boldsymbol{V}_n}\text{.}
\end{equation}
Due to the linearization, the covariance matrix $\boldsymbol{C}_{X_{(n|n-1)}}$ is not constant like in the linear case and must be evaluated for every step of the filter.
For Calculating $\hat{x}_{n|n}$, in equation \eqref{eq:x_nn} the term $\boldsymbol{H}_n \boldsymbol{\hat{x}}_{n|n-1}$ is replaced by the mean of $\boldsymbol{y}_n$
\begin{equation}
\label{eq: }
E[\boldsymbol{y}_n] = h(\hat{x}_{n-1|n-1})\text{.}
\end{equation}

The linear part of $\boldsymbol{y}_n$, $\boldsymbol{H}_n$,  can be calculated with the Jacobian matrix $J_h$ \eqref{eq:JacobianMatrix}.


\subsubsection{Multiplikative extended Kalman filter (MEKF)}
\label{sec:MEKF}
The \ac{MEKF} is an extension of the \ac{EKF} for attitude estimation, which uses unit quaternions as base for the attitude. The attitude is determined by three angles which means the covariance matrix has $\rang(\boldsymbol{C}_{V_n}) = 3$. Using quaternions to represent the attitude the covariance matrix is of size $4\times 4$ but the rang remains three\cite{Markley2003}, which results in a singularity when trying to invert the matrix. When implementing a quaternion based Kalman filter, it is assumed that the rang of the matrix is four.
%When unit quaternions are used as states in the filter, a singularity can occur while calculating the inverse in the measurement step of the Kalman filter, because the unit quaternions represent a three degrees of freedom attitude by using a four dimensional vector. This means the state covariance matrix has $\rang(\boldsymbol{Q}) = 3$ due to the three-dimensional rotation, but the state matrix dimension is $4\times 4$\cite{Markley2003}. 
To reduce the number of states for the attitude and getting a process covariance matrix of size $3\times 3$, not the unit quaternion itself, but the error between the real attitude and the estimated attitude is used as a state. The error quaternion $\delta \boldsymbol{q}$ is defined as the rotation from the estimated attitude to the real attitude
\begin{equation}
\label{eq:mekf_error}
\boldsymbol{q} = \hat{\boldsymbol{q}} \otimes \delta \boldsymbol{q}\text{.}
\end{equation}
The multiplikation of two unit quaternions is used, because adding two quaternions do not result anymore in a unit quaternion and the resulting quaternion must be normalized \cite{Maley2013}. The derivation of the attitude error unit quaternion is
\begin{equation}
\label{eq: }
\delta \dot{\boldsymbol{q}} = \hat{\boldsymbol{q}}^{-1} \otimes \dot{\boldsymbol{q}} + \dot{\hat{\boldsymbol{q}}}^{-1} \otimes \boldsymbol{q}
\end{equation}
with the inverse of the estimated attitude as
\begin{equation}
\label{eq: }
\dot{\hat{\boldsymbol{q}}}^{-1} = - \frac{1}{2} \begin{bmatrix}
0 \\
\hat{\omega}_b
\end{bmatrix} \otimes \hat{q}^{-1}\text{.}
\end{equation}
The error attitude can be simplyfied according to \cite{Maley2013} to 
\begin{equation}
\label{eq:mekfDeltaQ4}
\delta\dot{\boldsymbol{q}} = \begin{bmatrix}
0 \\
- \hat{\boldsymbol{\omega}} \times \delta \boldsymbol{q}_v
\end{bmatrix} + \frac{1}{2} \begin{bmatrix}
- \delta \boldsymbol{q}_v \cdot \delta \hat{\boldsymbol{\omega}} \\
\delta \hat{\boldsymbol{\omega}} + \delta \boldsymbol{q}_v \times \delta \boldsymbol{\hat{\omega}}
\end{bmatrix}\text{.}
\end{equation}
Due to the assumption of a small error between the estimated attitude and the real attitude, like already said in \eqref{eq:quaternionIdentity}, the error quaternion is almost an identity quaternion
\begin{equation}
\label{eq: }
\delta \boldsymbol{q} \approx \begin{bmatrix}
1 \\0 \\0\\0
\end{bmatrix}\text{.}
\end{equation}
In this case the dynamic of the scalar is ommited and only the vector part is relevant. The rotation matrix simplyfies to
\begin{equation}
\label{eq: }
\boldsymbol{R}_e^b (\delta \boldsymbol{q}) \approx \left[\boldsymbol{I} - 2\left[\delta \boldsymbol{q}_v \times \right]  \right]\text{.} 
\end{equation}
The quaternion error state $\delta\boldsymbol{q}$ is replaced by a vector of small angles $\boldsymbol{\alpha}$. There exist multiple representations of this vector\cite{Markley2003}. In this thesis $\boldsymbol{\alpha}$ is defined as in \cite{Maley2013} as
\begin{equation}
\label{eq: }
\boldsymbol{\alpha} = 2 \delta \boldsymbol{q}_v\text{.}
\end{equation}
The first benefit of this representation is, in equation \eqref{eq:mekfDeltaQ4} $\frac{1}{2}$ is ommited. The second advantage is the simplification of the calculation of the new estimated attitude, which is shown later. 
The derivative of this small angle vector is calculated according to\cite{Maley2013} to
\begin{equation}
\label{eq:mekfAlphaDot}
\dot{\boldsymbol{\alpha}} = - \hat{\boldsymbol{\omega}} \times \boldsymbol{\alpha} - \boldsymbol{b}_w\text{.}
\end{equation}
The rotation matrix can now be expressed in terms of the error angle $\boldsymbol{\alpha}$ and the estimated attitude $\hat{\boldsymbol{q}}$ from the previous step as
\begin{equation}%
\label{eq:mekfRetob}
\boldsymbol{R}_e^b (\boldsymbol{q}) \approx \left[\boldsymbol{I} - \boldsymbol{\alpha}_\times \right]   \boldsymbol{R}_e^b(\hat{\boldsymbol{q}}) 
\end{equation}
and
\begin{equation}
\label{eq:mekfRbtoe}
\boldsymbol{R}_b^e (\boldsymbol{q}) \approx \boldsymbol{R}_b^e(\hat{\boldsymbol{q}})\left[\boldsymbol{I} - \boldsymbol{\alpha}_\times  \right]\text{.}
\end{equation}
\nomenclature{$\boldsymbol{\alpha}$}{error angle used in the MEKF\nomunit{\si{-}}}
The estimated measurements must be calculated using equation \eqref{eq:mekfRetob} and \eqref{eq:mekfRbtoe} with the state $\boldsymbol{\alpha}$. After the measurement step of the \ac{EKF}, the new attitude is calculated from the previous estimated attitude and the small error angle. Due to the assumption, the small error is twice the vector part of the error quaternion, the new attitude can be calculated by calculating an unnormalized quaternion with
\begin{equation}
\label{eq:mekfUpdateAttitude1}
\boldsymbol{p} = \hat{\boldsymbol{q}}_{k-1} \otimes \begin{bmatrix}
2 \\ \boldsymbol{\alpha}
\end{bmatrix}
\end{equation}
and then renormalizing 
\begin{equation}
\label{eq:mekfUpdateAttitude2}
\hat{\boldsymbol{q}}_{k} = \frac{\boldsymbol{p}}{||\boldsymbol{p}||}\text{.}
\end{equation}
The mean of the attitude error angle is zero, which means, the small error angle must be set to zero after the predictor step. This is different to the papers \cite{Maley2013,PhilippeMartin2010,Markley2003}, because they only use errors between the real value and the estimated value as states. In this thesis, only for the attitude an error value is used as a state. For other states like the velocity or the position the velocity/position itself is the state and these states must be predicted in the prediction step of the \ac{EKF}. The following table summarizes the steps of the \ac{MEKF}
\begin{table}
	\centering
	\begin{tabular}{ | c | l |}
		\hline
		1 & calculate $\hat{\boldsymbol{q}}_{k+1}$ with equation \eqref{eq:quaternionIntegration} \\
		2 & \ac{EKF} prediction state using the equations defined in section \ref{sec:KalmanFilter} \\
		3 & reset all error states to zero \\
		4 & \ac{EKF} measurement step \\
		5 & update $\hat{\boldsymbol{q}}_{k+1}$ with the error state $\boldsymbol{\alpha}$ with equations \eqref{eq:mekfUpdateAttitude1} and \eqref{eq:mekfUpdateAttitude2} \\
		\hline
	\end{tabular}
\caption{Steps of the \ac{MEKF}.}
\end{table}

