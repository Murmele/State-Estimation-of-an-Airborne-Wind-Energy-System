\chapter{Simulations}
\label{chap:Simulations}
The input data for the filter was simulated by F. Bauer. The trajectory was chosen to convert as much wind energy as possible into electrical energy. The position and the velocity of the simulated kite can be seen in figure \ref{fig:inputDataPosVel}. From $\SI{80}{s}$ on the kite is in cross wind flight and flies a periodic trajectory and produces electrical energy. This can be read out from the third diagram of figure \ref{fig:inputDataAttitude}. From this time on, there is a high  acceleration part which is measured from the kite accelerometer. In figure \ref{fig:inputDataAttitude} the attitude of the kite represented by three Euler angles is shown. The variances used in the Kalman filter have the values from table \ref{tab:variancesSensors} and \ref{tab:variancesStates}. These values are used in the Pixhawk 4 firmware\cite{PX4DroneAutopilotContributors2019}. The gyroscope bias, magnetometer, barometer and GPS variances are used from Jeffrey D. Barton\cite{Barton2012}.
\begin{figure}[ht]
	\centering
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/InputData/BauerDatenWithoutINCDEC}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/InputData/BauerDatenWithoutINCDEC2.tex}
	\caption{Position and velocity of the kite.}
	\label{fig:inputDataPosVel}
\end{figure}
\begin{figure}[ht]
	\centering
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/InputData/BauerDatenAttitude}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/InputData/BauerDatenAttitude2.tex}
	\caption{Attitude of the kite.}
	\label{fig:inputDataAttitude}
\end{figure}
\begin{table}[ht]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		Sensor & Variance \\
		\hline
		Gyroscope & \SI{2,5e-4}{\radian\per\second} \\
		Accelerometer & \SI{0,1225}{\meter\per\square\second} \\
		Magnetometer & \SI{3,24e-2}{\micro\tesla}\\
		GPS Position & \SI{10}{\meter} \\
		GPS Velocity & \SI{1,5}{\meter\per\second} \\
		Barometer & \SI{0,66}{\meter} \\
		Line length & \SI{0,3}{\meter} \\
		Line angle sensor & \SI{3}{\degree}\\
		\hline
	\end{tabular}
	\caption{Sensor precision.}
	\label{tab:variancesSensors}
\end{table}
The variance for the Euler angles and the quaternion states come from the gyroscope due to the equations \eqref{eq:quaternion_derivative} and \eqref{eq:gimbal_lock}.
\FloatBarrier
\begin{table}[ht]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		State variable& Variance $\sigma $ & Annotation\\
		\hline
		Euler angles $\boldsymbol{\gamma}$& \SI{2,5e-4}{\radian\per\second} & Comes from the gyroscope \\
		quaternion $\boldsymbol{q}$& \SI{2,5e-4}{\radian\per\second} & Comes from the gyroscope \\
		Gyroscope bias $\boldsymbol{b}_w$& \SI{1e-6}{\radian\per\second} & Used from \cite{Barton2012}\\
		Attitude error $\boldsymbol{\alpha}$ & \SI{2,26e-4}{\radian\per\second} & Gyroscope plus Gyroscope bias\\
		\hline
	\end{tabular}
	\caption{State precision.}
\label{tab:variancesStates}
\end{table}
In the next sections some filters are presented and evaluated. To evaluate the errors between the real value and the estimated value, the \ac{RMSD} function is used\\
\begin{equation}
\label{eq:RMSD}
\rmsd(y) = \sqrt{\frac{\sum_{t=1}^{T}\left( \hat{y}_t - y_t \right)^2 }{T}}\text{.}
\end{equation}
In all of this filter the following assumptions are used
\begin{assumption}
	The gyroscope bias is constant or slowly varying\cite{1582367}.
	\begin{equation}
	\label{eq: }
	\dot{\boldsymbol{b}}_\omega \approx \boldsymbol{0}
	\end{equation}
\end{assumption}
\begin{assumption}
	The velocity of the kite in body coordinates $\boldsymbol{v}_b$ can be calculated from the inertia velocity as
	\begin{equation}
	\label{eq:v_b}
	\boldsymbol{v}_b = \begin{bmatrix}
	||\boldsymbol{v}_e|| \\ 0 \\ 0
	\end{bmatrix}
	\end{equation}%
	because it is assumed, that the kite flies always in the direction of the body $x$ axis.
\end{assumption}
It's not possible to find the variance of $p_{tether}$ algebraically due to the nonlinearity. In this case the variance can be calculated by linearizing the position and then calculating the variance in a single point or by creating a simulation of the position at specific angles and tether length and calculating the variance of all of these points. In this work it's assumed, that the variance, estimated with a simulation at a specific angle is the maximum variance which can be found.
\begin{assumption}
	The variance of the tether angle position estimation is constant.
\end{assumption}
In the first part of this chapter until section \ref{sec:tetherSag} it's also assumed
\begin{assumption}
	The tether is a straight line whereas the position can be calculated as in \ref{sec:tetherAngleSensor}.
\end{assumption}
\section{\ac{AHRS} filter}

\subsection{Euler angle based \ac{AHRS} \ac{EKF}}
\label{sec:eulerAHRSEKF}
The \ac{AHRS} \ac{EKF} based on Euler angles as states is the simplest filter in this thesis. The attitude is directly visible in the states, which makes debugging and checking the filter quite easy. The drawback is, as  already described in \ref{subsec:euler_angles} a gimbal lock can occur and the filter can fail.
As states the attitude and the biases $\boldsymbol{b}_w \in \mathbb{R}^3 $ of the angular velocity are used. The biases are used, because they are important for integrating the angular velocity to get the attitude. As measurements, the accelerometer and the magnetometer are used. The angular rate is used in the update step to estimate the new attitude. The \ac{GPS} velocity is used to compensate the centripetal acceleration. The state equation is defined as
\begin{equation}
\label{eq: }
\begin{bmatrix}
\dot{\boldsymbol{\gamma}}\\
\dot{\boldsymbol{b}}_w
\end{bmatrix} = \begin{bmatrix}
\boldsymbol{S} (\boldsymbol{\omega}_b - \boldsymbol{b}_w) \\
\boldsymbol{0}
\end{bmatrix}
\end{equation}
with $\boldsymbol{\gamma}$ the attitude and $\boldsymbol{b}_\omega$ the gyroscope bias. The measurement equation is expressed as
\begin{equation}
\label{eq:eulerAHRSEKF_measurementEstimation}
\begin{bmatrix}
\boldsymbol{a}_b \\
\boldsymbol{m}_b
\end{bmatrix} = \begin{bmatrix}
\hat{\boldsymbol{R}}_e^b(\boldsymbol{\gamma}) \boldsymbol{g}_e + (\boldsymbol{\omega}_b - \boldsymbol{b}_w)\times \boldsymbol{v}_b \\
\hat{\boldsymbol{R}}_e^b \boldsymbol{m}_e
\end{bmatrix}
\end{equation}%
\nomenclature{$\boldsymbol{\gamma}$}{$\in \mathbb{R}^3$ $\begin{bmatrix}
	\phi & \theta & \psi
\end{bmatrix}^T$ , attitude of the kite\nomunit{\si{\radian}}}%
with the estimated rotation matrix $\hat{\boldsymbol{R}}_e^b$. Comparing the upper part of the right hand side of equation \eqref{eq:eulerAHRSEKF_measurementEstimation} and \eqref{eq:acceleration} which is
\begin{equation}
\label{eq: }
\boldsymbol{a}_b = \dot{\boldsymbol{v}}_b + \boldsymbol{w}_b \times \boldsymbol{v}_b - \boldsymbol{g}_b\text{,}
\end{equation}
it is visible, that the first part of the equation, the translational acceleration, is missing. Because of this, at high translational accelerations high errors in the attitude exist which is visible in figure \ref{fig:eulerAHRSEKF}. Due to the low sample rate of the \ac{GPS} module and the fact that differentiating noisy signals is not a good choice, it's impossible to estimate the translational acceleration. At low translational accelerations like at the start phase of the kite until about $\SI{80}{\second}$ the error is low and the estimated gyro bias converges to the real gyro bias seeing in figure \ref{fig:eulerAHRSEKF} and \ref{fig:eulerAHRSEKFBias}.
The position is estimated from the \ac{GPS} module. As seen in figure \ref{fig:eulerAHRSEKF}, the filter does not touch the angles \SI{\pm 90}{\degree} for the angle $\theta$, for what reason no singularities occurred. Seeing in figure \ref{fig:eulerAHRSEKFBias}, at high accelerations, the estimation of the gyro bias is completely failing.
\begin{figure}[ht]
	\centering
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/ahrsEKF/angles}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/ahrsEKF/angles2.tex}
	\caption{Simulation result of the Euler \ac{AHRS} \ac{EKF}.}
	\label{fig:eulerAHRSEKF}
\end{figure}

\begin{figure}[ht]
	\centering
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/ahrsEKF/estBias}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/ahrsEKF/estBias2.tex}
	\caption{Estimated gyroscope bias. Due to the high translational acceleration, the gyro bias and the attitude have high errors.}
	\label{fig:eulerAHRSEKFBias}
\end{figure}

\FloatBarrier

\subsection{Quaternion based \ac{AHRS} \ac{EKF}}
The \ac{AHRS} \ac{EKF} based on quaternions has now four states for the representation of the attitude, but does not have the problem singularities occure like for the Euler based \ac{EKF}. The state equation is defined as
\begin{equation}
\label{eq: }
\dot{\boldsymbol{x}}=\begin{bmatrix}
\dot{\boldsymbol{q}} \\
\dot{\boldsymbol{b}_w}
\end{bmatrix} = \begin{bmatrix}
\frac{1}{2} \boldsymbol{q} \begin{bmatrix}
0 \\
\boldsymbol{\omega}_b - \boldsymbol{b}_w
\end{bmatrix}
\\
\boldsymbol{0}
\end{bmatrix}\text{.}
\end{equation}
The measurement equations are defined similar to the Euler based \ac{AHRS} \ac{EKF} with the difference, equation \eqref{eq:quaterion_matrix_frame_change} is used to rotate the gravity / magnetic field vector into the body frame
\begin{equation}
\label{eq: }
\boldsymbol{y} = \begin{bmatrix}
\boldsymbol{a}_b \\
\boldsymbol{m}_b
\end{bmatrix} = \begin{bmatrix}
R_e^b(\boldsymbol{\hat{q}})g_e + (\boldsymbol{\omega}_b - \boldsymbol{b}_\omega) \times \boldsymbol{v}_b\\
R_e^b(\boldsymbol{\hat{q}})m_e
\end{bmatrix}\text{.}
\end{equation}
The derivative of the quaternion was already defined in equation \eqref{eq:quaternion_derivative}. Comparing the estimated angles of the Euler based \ac{AHRS} \ac{EKF} from \ref{sec:eulerAHRSEKF} in figure \ref{fig:eulerAHRSEKF} it is visible, that the characteristic of the filters are the same. Like the euler \ac{AHRS} \ac{EKF}, the quaternion \ac{AHRS} \ac{EKF} does not estimate well, when high translational acceleration is present.                                              
\begin{figure}[ht]
	\centering
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/ahrsQuaternion/angles}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/ahrsQuaternion/angles2.tex}
	\caption{Quaternion based \ac{AHRS} \ac{EKF}.}
	\label{fig:quaternionAHRSEKF}
\end{figure}
\FloatBarrier
\subsection{AHRS \ac{MEKF}}
Following the description in \ref{sec:MEKF} a \ac{MEKF} was developed to estimate the attitude of the kite. The system and measurement equations are defined similar to the Euler and quaternion \ac{EKF}
\begin{equation}
\label{eq: }
\dot{\boldsymbol{x}} = \begin{bmatrix}
\dot{\boldsymbol{\alpha}} \\
\dot{\boldsymbol{b}}_w
\end{bmatrix} = \begin{bmatrix}
- (\boldsymbol{\omega}-\boldsymbol{b}_w)_\times & -\boldsymbol{I}_3 \\
\boldsymbol{0} & \boldsymbol{0}
\end{bmatrix} \begin{bmatrix}
\boldsymbol{\alpha} \\
\boldsymbol{b}_w
\end{bmatrix}
\end{equation}

\begin{equation}
\label{eq: }
\boldsymbol{y} =\begin{bmatrix}
\boldsymbol{a}_b \\
\boldsymbol{m}_b
\end{bmatrix} = \begin{bmatrix}
(\boldsymbol{I}_3-\boldsymbol{\alpha}_\times ) \boldsymbol{R}_e^b (\hat{\boldsymbol{q}}) \boldsymbol{g}_e + (\boldsymbol{\omega}-\boldsymbol{b}_w)\boldsymbol{v}_b\\
(\boldsymbol{I}_3-\boldsymbol{\alpha}_\times )\boldsymbol{R}_e^b (\hat{\boldsymbol{q}}) \boldsymbol{m}_e
\end{bmatrix}
\end{equation}
with $\boldsymbol{\alpha}$ the small error angle vector. The result of this filter is shown in figure \ref{fig:MEKFAHRSresult}.
\begin{figure}[ht]
	\centering
	\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/ahrsMEKF/mekf}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/ahrsMEKF/mekf2.tex}
	\caption{Attitude estimation of an \ac{AHRS} \ac{MEKF}.}
	\label{fig:MEKFAHRSresult}
\end{figure}
The result is similar to the results from the other \ac{AHRS} filters, but with the advantage to not have any singularities like the quaternion \ac{AHRS} \ac{EKF} and having only three states ($\boldsymbol{\alpha}$) for the attitude which increases the performance of the filter.
\FloatBarrier
\subsection{Explicit complementary filter}
\label{sec:MahonyOptParameter}
Seeing in figure \ref{fig:MahonyFilterResult}, the filter is in the starting phase quite good and similar to the \ac{EKF} filter, but when high translational acceleration is present, the filter is unusable. This filter cannot be extended to be an \ac{INS} filter. The parameters of the filter are determined by optimizing the filter on this trajectory, with the weight for the importance of the error angle as described in table \ref{tab:mahonyFilterWeights}.
\begin{table}[ht]
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		Weight & $\phi$ & $\theta$ & $\psi$ \\
		\hline
		& 	1 & 1 & 0.5 \\
		\hline
	\end{tabular}
	\caption{Weights of the explicit complementary filter optimization.}
	\label{tab:mahonyFilterWeights}
\end{table}
The weight for the yaw ($\psi$) angle is choose lower than the roll($\phi$) and pitch ($\theta$) angle, because it's more important of being stable in x and y axis.
\begin{figure}[ht]
	\centering
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/MahonyExplicitComplementaryFilter/angle}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/MahonyExplicitComplementaryFilter/angle2.tex}
	\caption{Attitude estimation of the explicit complementary filter.}
	\label{fig:MahonyFilterResult}
\end{figure}

\FloatBarrier
\section{\ac{INS} filter}
To avoid the problem of the filters at high translational accelerations, an \ac{INS} filter can be used. This filter uses the velocity as additional state, whereas the estimated acceleration is not anymore used in the correction step, but the measured acceleration is rotated into the earth frame and used in the state equation of the velocity instead of the measurement equations. The following equations use the \ac{MEKF} approach described in section \ref{sec:MEKF} but the \ac{INS} filter can also be described in Euler or quaternion representation. The state equation is defined as
\begin{equation}
\label{eq:INSStateEquation}
\dot{\boldsymbol{x}} =  \begin{bmatrix}
\dot{\boldsymbol{\alpha}} \\
\dot{\boldsymbol{p}} \\
\dot{\boldsymbol{v}} \\
\dot{\boldsymbol{b}}_w
\end{bmatrix} = \begin{bmatrix}
- (\boldsymbol{\omega}-\boldsymbol{b}_w)_\times \boldsymbol{\alpha}-\boldsymbol{b}_w\\
\boldsymbol{v} \\
\boldsymbol{R}_b^e(\hat{\boldsymbol{q}})(\boldsymbol{I}_3 + \boldsymbol{\alpha}_\times)\boldsymbol{a}_b + \boldsymbol{g}_e\\
\boldsymbol{0}
\end{bmatrix}\text{.}
\end{equation}
The measurement equation reads as 

\begin{equation}
\label{eq:INSMeasurementEquation}
\boldsymbol{y} = \begin{bmatrix}
\boldsymbol{m}_b \\
h_{baro} \\
\boldsymbol{p}_{GPS} \\
\boldsymbol{p}_{line} \\
\boldsymbol{v}_{GPS}
\end{bmatrix} = \begin{bmatrix}
(\boldsymbol{I}_3 - \boldsymbol{\alpha}_\times)\boldsymbol{R}_e^b(\hat{\boldsymbol{q}})\boldsymbol{m}_e \\
p_3\\
\boldsymbol{p} \\
\boldsymbol{p} \\
\boldsymbol{v}
\end{bmatrix}\text{.}
\end{equation}
As visible in the measurement equation, multiple sensors measure the position of the kite. This results in estimating the mean of all of this sensors position with the weighting of the covariance matrix entries (see table \ref{tab:variancesSensors}). 
The results of the simulation can be seen in figure \ref{fig:INSMEKFResultAngle}, \ref{fig:INSMEKFResultGyroBias} and \ref{fig:INSMEKFResultPosVel}. The filter is much better than the \ac{AHRS} filters because the translational acceleration is now respected. The gyroscope bias is estimated very well, seen in figure \ref{fig:INSMEKFResultGyroBias}.
\begin{figure}[ht]
	\centering
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/INSMEKF/angles}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/INSMEKF/angles2.tex}
	\caption{Estimated attitude.}
	\label{fig:INSMEKFResultAngle}
\end{figure}
\begin{figure}[!ht]
	\centering
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/INSMEKF/gyroBias}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/INSMEKF/gyroBias2.tex}
	\caption{Estimated gyroscope bias.}
	\label{fig:INSMEKFResultGyroBias}
\end{figure}
\begin{figure}[ht]
	\centering
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/INSMEKF/posVel}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/INSMEKF/posVel2.tex}
	\caption{Estimated position and velocity.}
	\label{fig:INSMEKFResultPosVel}
\end{figure}


\FloatBarrier
\section{Additional limitations}
In this section additional limitations are discussed and implemented into the filter to test the performance.

\subsection{Loop time shorter than sensor reading}
\label{sec:DST}
Some sensors do not have such high sample rate than the  filter. For this case the simulation was extended to use the sample rates in table \ref{tab:sampleRates}. The sample rate of the filter is the same as the gyroscope one.
\begin{table}[ht]
	\centering
	\begin{tabular}{|l|c|}
		\hline
		Sensor / Filter & Sample Rate \\
		\hline
		Filter loop time & \SI{100}{Hz} \\
		Gyroscope & \SI{100}{Hz} \\
		Accelerometer & \SI{100}{Hz} \\
		Magnetometer & \SI{100}{Hz} \\
		GPS & \SI{10}{Hz} \\
		Barometer & \SI{100}{Hz} \\
		Line angle sensor & \SI{100}{Hz}\\
		\hline
	\end{tabular}
	\caption{Sample rates of different sensors.}
	\label{tab:sampleRates}
\end{table}
Three different concepts for handling different sample times are evolved. The first filter uses the previous value when no new measurement is available, the second filter skips the correction step of the not available filter and the third filter integrates the position from the velocity, and uses the previous values for magnetometer, accelerometer and gyroscope. The results of the simulations are visible in table \ref{tab:sampleRateSimulation}, \ref{tab:sampleRateSimulationPos} and \ref{tab:sampleRateSimulationVel}. Seeing in this tables, the attitude estimations from INSMEKFDST and INSMEKF Interpolate are better than ignoring the availability and using old values. Interpolating the position or the velocity in high dynamic trajectories is not a good idea. Better it's to skip the correction step of the \ac{GPS} module if no measurements are available and just using the tether angle sensor to estimate the position.

\begin{figure}[ht]
	\centering
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/DifferentSampleTime/DST}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/DifferentSampleTime/DST2.tex}
	\caption{Attitude Estimation with different sensor availability handling.}
	\label{fig:DST}
\end{figure}

%\begin{table}[ht]
%	\centering
%	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
%		\hline
%		Filter & Name in \ref{fig:DST}&\multicolumn{3}{c}{Angle [\si{\degree}]} \vline \\
%		\hline
%		& &$\rmsd(\phi )$& $\rmsd(\theta )$ & $\rmsd(\psi )$ \\
%		\hline
%		\makecell{\ac{INS} \ac{MEKF} \\ without interpolation} & INSMEKF  & 1.9754&	0.4728&	2.7112	\\
%		\hline
%		\makecell{	\ac{INS} \ac{MEKF} \\ no measurement step \\if no measurement available} & INSMEKFDST & 2.8134&	0.7332&	5.7199 \\
%		\hline
%		\makecell{	\ac{INS} \ac{MEKF} \\ with interpolation} &\makecell{INSMEKF\\Interpolate}& 1.9982&	0.5163&	2.8072 \\
%		\hline
%	\end{tabular}
%	\caption{Estimated angle error with different sensor availability handling}
%	\label{tab:sampleRateSimulation}
%\end{table}

\begin{table}[ht]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline
		Filter & Name in \ref{fig:DST}&\multicolumn{3}{c}{Angle [\si{\degree}]} \vline \\
		\hline
		& &$\rmsd(\phi )$& $\rmsd(\theta )$ & $\rmsd(\psi )$ \\
		\hline
		\input{MatlabTableExport/sampleRateSimulationAngle2019-05-18_10-04-53.LatexTable}
	\end{tabular}
	\caption{Estimated angle error with different sensor availability handling.}
	\label{tab:sampleRateSimulation}
\end{table}

\begin{table}[ht]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline
		Filter & Name in \ref{fig:DST}&\multicolumn{4}{c}{Position [\si{\meter}]} \vline\\
		\hline
		&  & $\rmsd( x)$& $\rmsd(y)$ & $\rmsd(z)$ & $\rmsd(||.||)$ \\
		\hline
		\input{MatlabTableExport/sampleRateSimulationPos2019-05-18_10-04-53.LatexTable}
	\end{tabular}
	\caption{Estimated position error with different sensor availability handling.}
	\label{tab:sampleRateSimulationPos}
\end{table}

\begin{table}[!ht]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline
		Filter & Name in \ref{fig:DST}& \multicolumn{4}{c}{Velocity [\si{\meter\per\second}]} \vline \\
		\hline
		& & $\rmsd( x)$& $\rmsd(y)$ & $\rmsd(z)$ & $\rmsd(||.||)$ \\
		\hline
		\input{MatlabTableExport/sampleRateSimulationVel2019-05-18_10-04-53.LatexTable}
	\end{tabular}
	\caption{Estimated velocity error with different sensor availability handling.}
	\label{tab:sampleRateSimulationVel}
\end{table}
The interpolation of the position is done with the formula
\begin{equation}
\label{eq: }
p = p_x + v_{GPS} \cdot sampleTime
\end{equation}
where x is a placeholder for GPS, lineAngle or barometer.
\FloatBarrier
\subsection{GPS module acceleration limitation}
Cheap \ac{GPS} modules are limited to certain parameters like height, velocity and acceleration. As example, the u-blox NEO-6 module is limited to an acceleration of \SI{4}{g}\cite{u-blox2019}. The dynamics of the kite is high enough to exceed this limit and so no \ac{GPS} data are anymore available. This limitation was simulated and can be seen in figure \ref{fig:GPSLimitPosition}, where the \ac{GPS} position is constant for the time the \SI{4}{g} acceleration is exceeded. The position is constant, because the previous value is used for the filter. The time range between two \ac{GPS} points is about \SI{1,2}{\second}. Seeing in figure \ref{fig:GPSLimitAcc} or in table \ref{tab:angle4gLimit}, \ref{tab:pos4gLimit} and \ref{tab:vel4gLimit} it is visible that the estimation errors for the filter which uses the previous values and the filter which interpolates the position increased a lot compared to the results from section \ref{sec:DST}. The filter which skips the correction step do not increase much. This is because if no \ac{GPS} is available, the tether angle sensor is used. To interpolate the missing values decreases the error, but skipping the measurements which are not available gives much better results.
\begin{figure}[ht]
	\centering
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/AccelerationLimitedGPSModule/GPSPos.tex}
	\includegraphics[width=0.7\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/AccelerationLimitedGPSModule/GPSPos}
	\caption{GPS Position with limitation to \SI{4}{g}.}
	\label{fig:GPSLimitPosition}
\end{figure}
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.7\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/AccelerationLimitedGPSModule/INSMEKF}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/AccelerationLimitedGPSModule/INSMEKF2.tex}
	\caption{Estimated angles of different filters and the error for the filter which does not calculate the correction step, with the \ac{GPS} module limited to an acceleration of \SI{4}{g} and sample rates defined in section \ref{sec:DST}.} 
	\label{fig:GPSLimitAcc}
\end{figure} 
\begin{figure}[ht]
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/AccelerationLimitedGPSModule/Pos}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/AccelerationLimitedGPSModule/Pos2.tex}
	\caption{Position of INSMEKFDST, with the GPS module limited to an acceleration of \SI{4}{g}.}
	\label{fig:GPSLimitEstimation}
\end{figure} 
\nomenclature{INSMEKFDST}{\ac{INS} filter based on the \ac{MEKF} which does not calculate the correction step for the non available filter}
\begin{table}[ht]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline
		Filter & Name in \ref{fig:GPSLimitEstimation}&\multicolumn{3}{c}{Angle [\si{\degree}]} \vline \\
		\hline
		& &$\rmsd(\phi )$& $\rmsd(\theta )$ & $\rmsd(\psi )$ \\
		\hline
		\input{MatlabTableExport/angle4gLimit2019-05-18_10-08-07.LatexTable}
	\end{tabular}
	\caption{Estimated angle error with \SI{4}{g} limitation of the GPS module.}
	\label{tab:angle4gLimit}
\end{table}

\begin{table}[ht]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline
		Filter & Name in \ref{fig:GPSLimitEstimation}&\multicolumn{4}{c}{Position [\si{\meter}]} \vline\\
		\hline
		&  & $\rmsd( x)$& $\rmsd(y)$ & $\rmsd(z)$ & $\rmsd(||.||)$ \\
		\hline
		\input{MatlabTableExport/pos4gLimit2019-05-18_10-08-07.LatexTable}
	\end{tabular}
	\caption{Estimated position error with \SI{4}{g} limitation of the GPS module.}
	\label{tab:pos4gLimit}
\end{table}

\begin{table}[ht]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline
		Filter & Name in \ref{fig:GPSLimitEstimation}& \multicolumn{4}{c}{Velocity [\si{\meter\per\second}]} \vline \\
		\hline
		& & $\rmsd( x)$& $\rmsd(y)$ & $\rmsd(z)$ & $\rmsd(||.||)$ \\
		\hline
		\input{MatlabTableExport/vel4gLimit2019-05-18_10-08-07.LatexTable}
	\end{tabular}
	\caption{Estimated velocity error with \SI{4}{g} limitation of the GPS module.}
	\label{tab:vel4gLimit}
\end{table}

\FloatBarrier

\subsection{Tether sag}
\label{sec:tetherSag}
In the previous sections it was assumed, the tether is a straight line and the position could be calculated with the equation \eqref{eq:tetherAngleSensorPositionCalculation} from section \ref{sec:tetherAngleSensor}. Due to the mass of the tether it's impossible to have a straight line and the tether has a sag. To determine the position of the kite including the compensation of the tether sag, the tether is described as a Catenary curve \cite{Center2015}. This type of curve was developed to describe cables hanging under their own weight\cite{Center2015}. This derivation is first done in 2D which can be seen in figure \ref{fig:tetherSag} and will be extended to 3D in a second step. The Catenary curve is described in general as
\begin{equation}
\label{eq:catenary}
y = C2 + a \cosh\left(\frac{1}{a} x + C1\right)\text{.}
\end{equation}
There are three parameters $a$, $C1$ and $C2$ which must be determined to estimate the position of the kite. $C1$ and $C2$ are constants of integration and can be found by using defined boundaries. In general the derivative of the Catenary curve is
\begin{equation}
\label{eq:cateneryDiff}
\frac{dy}{dx} = \sinh\left(\frac{1}{a} x + C1\right)\text{.}
\end{equation}
The line angle sensor measures the tether angle and therefore the gradient of the tether is
\begin{equation}
\label{eq:catenaryLineAngleBase}
\tan(\theta_{base}) = \frac{y}{x}\text{.}
\end{equation}
$C1$ can be found by using the line angle sensor at the base station \eqref{eq:catenaryLineAngleBase} and the derivative of the Catenary curve \eqref{eq:cateneryDiff} with $x=0$ as
\begin{equation}
\label{eq:catenaryC1}
C1 = \asinh\left(\tan(\theta_{base})\right)\text{.}
\end{equation}
The second integration constant can be calculated with the fact that the tether is connected to the base, which means at position $x=0$, $y=0$ holds. Plugging these values into the Catenary equation \eqref{eq:catenary} $C2$ is expressed in terms of $C1$ as
\begin{equation}
\label{eq: }
C2 = -a \cosh(C1)\text{.}
\end{equation}
The constant $a$ can be calculated in dependence of the kite horizontal position $x_{kite}$ from the second tether angle sensor on the kite as
\begin{equation}
\label{eq: }
a = \frac{x_{kite}}{\asinh\left(\tan(\theta_{kite})\right)- C1}\text{.}
\end{equation}
As the length of the tether is known, it's possible to use an algebraic equation of the curve length to determine the horizontal distance of the kite. In general a curve length can be determined with the formula from \cite{Dawkins2018} as
\begin{equation}
\label{eq: }
l = \int_{0}^{x_{kite}}\sqrt{1+f'(x)^2} dx\text{.}
\end{equation}%
\nomenclature{$\theta_{kite}$}{azimut angle measured with the line angle sensor in the kite\nomunit{\si{\radian}}}%
\nomenclature{$\phi_{kite}$}{elevation angle measured with the line angle sensor in the kite\nomunit{\si{\radian}}}%
Using this formula and plugging the definitions of $C1$, $C2$ and $a$ into the result we get the following expression for $x_{kite}$
\begin{equation}
\label{eq:catenaryX_kite}
x_{kite} = \frac{2 l\, {{e}^{\operatorname{asinh}\left( \tan(\theta_{kite})\right) +\mathit{C1}}} \operatorname{asinh}\left( \tan(\theta_{kite})\right) -2 \mathit{C1} l\, {{ e}^{\operatorname{asinh}\left( \tan(\theta_{kite})\right) +\mathit{C1}}}}{{{e}^{2 \operatorname{asinh}\left( \tan(\theta_{kite})\right) +\mathit{C1}}}-{{e}^{\operatorname{asinh}\left( \tan(\theta_{kite})\right) +2 \mathit{C1}}}+{{e}^{\operatorname{asinh}\left( \tan(\theta_{kite})\right) }}-{{ e}^{\mathit{C1}}}}\text{.}
\end{equation}
The y value is then calculated with the Catenary curve.
\paragraph{Extending to 3D}
The Catenary curve is a two dimensional curve, but the position of the kite is in three-dimensional space. For this reason, the $x$ and $y$ positions are combined to get a horizontal distance as
\begin{equation}
\label{eq:catenaryX_hor}
x_{hor} = \sqrt{x^2 + y^2},
\end{equation}
the $z$ component is the $y$ component in the two-dimensional case. $x_{hor}$ can be calculated with formula \eqref{eq:catenaryX_kite}. $z$ can then determined with the catenary equation \eqref{eq:catenary}. The $x$ and $y$ component can be calculated with the help of the azimut angle $\phi_{base}$, measured from the base as
\begin{equation}
\label{eq: }
x = x_{hor} \cos(\phi_{base})
\end{equation}
and
\begin{equation}
\label{eq: }
y = x_{hor} \sin(\phi_{base})\text{.}
\end{equation}

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\columnwidth ]{../Bilder/tetherSag} \\
	\small Source: Adapted from \cite{Auawise2019}
	\caption{Tether sagging due to the mass of the tether.}
	\label{fig:tetherSag}
\end{figure}
\paragraph{Simulating tether sag}
Because no tether angle values in the received simulation data are available, the angles must be simulated. For this the previous steps must be performed in reverse order. Therefore, we introduce a new variable to increase the calculation speed by not calculating every time the sinus hyperbolicus
\begin{equation}
\label{eq:catenaryB}
b = \asinh(\tan(\theta_{kite}))\text{.}
\end{equation}
To map the three-dimensional position to a two-dimensional system we use the horizontal position $x_{hor}$. With the new variable $b$ and the horizontal position $x_{hor}$ we get $a$ as
\begin{equation}
\label{eq: }
a = \frac{x_{hor}}{b - C1}
\end{equation}
Putting this equation in the Catenary curve and setting the $z$ component of the position as $y$ we get
\begin{equation}
\label{eq:catenaryInvGl1}
-\mathit{z}+\frac{\cosh{(b)} \mathit{x_{hor}}}{b-\mathit{C1}}-\frac{\cosh{\left( \mathit{C1}\right) } \mathit{x_{hor}}}{b-\mathit{C1}} = 0
\end{equation}
 with the unknowns $b$ and $C1$. To get a solution for $C1$ and $b$ we use the curve length equation to get a second equation
\begin{equation}
\label{eq:catenaryInvGl2}
l-\frac{e^{2b+C1}x1+(1-e^{2C1})e^bx_{hor}-e^C1x_{hor}}{2e^{b+C1}b-2C1e^{b+C1}} = 0\text{.}
\end{equation}
There is no algebraic solution of this nonlinear equation system, for what reason, this system must be solved numerically. After solving this system, $\theta_{kite}$ and $\theta_{base}$ can be calculated with equation \eqref{eq:catenaryB} and \eqref{eq:catenaryC1}. The azimut angle of the base tether angle sensor can be calculated as
\begin{equation}
\label{eq: }
\phi_{base} = tan\left( \frac{y}{x} \right) \text{.}
\end{equation}
\subsubsection{Simulation results}
For the simulation the following assumption is used
\begin{assumption}
	The tether lenght is \SI{155}{\meter} at a direct distance from the base station to the kite of \SI{150}{\meter}
\end{assumption}
The results of the above compensation can be seen in table \ref{tab:AngletetherSagComp}, \ref{tab:PostetherSagComp} and \ref{tab:VeltetherSagComp}. The attitude is not falsified much from the sag, but the errors of the $x$ and $y$ position of the kite are falsified, seeing also in figure \ref{fig:AngletetherSagComp}. This is, because the position of the line angle sensor is estimated wrong and has a much higher sample frequency as the \ac{GPS} module, whereas, the position is next to the estimated position from the line angle sensor. The $z$ component is not much falsified, because the barometer has a lower variance than the \ac{GPS} module and the tether angle sensor. The estimation is near to the estimation of the barometer. The velocity is not falsified, because this state is only compared to the \ac{GPS} velocity.
\begin{table}[ht]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline
		Filter &\multicolumn{3}{c}{Angle [\si{\degree}]} \vline \\
		\hline
		& $\rmsd(\phi )$& $\rmsd(\theta )$ & $\rmsd(\psi )$ \\
		\hline
%		\makecell{\ac{INS} \ac{MEKF} \\ without tether sag compensation}  & 2.6185	&1.0773&	4.8496	\\
%		\makecell{\ac{INS} \ac{MEKF} \\ with tether sag compensation}  & 2.9577	&0.9084&	6.2058	\\
%		\hline
		\input{MatlabTableExport/AngletetherSagComp2019-05-18_16-07-47.LatexTable}
	\end{tabular}
	\caption{Estimated angle error with and without tether sag compensation.}
	\label{tab:AngletetherSagComp}
\end{table}
\begin{table}[ht]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline
		Filter & \multicolumn{4}{c}{Position [\si{\meter}]} \vline\\
		\hline
		&   $\rmsd( x)$& $\rmsd(y)$ & $\rmsd(z)$ & $\rmsd(||.||)$ \\
		\hline
%		\makecell{\ac{INS} \ac{MEKF} \\ without tether sag compensation}  & 61.3514	&62.8277	&0.1327&	87.8142	\\
%		\hline
%		\makecell{	\ac{INS} \ac{MEKF} \\ with tether sag compensation} & 0.5494	&0.5454&	0.1095&	0.7818\\
%		\hline
		\input{MatlabTableExport/PostetherSagComp2019-05-18_16-07-47.LatexTable}
	\end{tabular}
	\caption{Estimated position error with and without tether sag compensation.}
	\label{tab:PostetherSagComp}
\end{table}
\begin{table}[ht]
	\centering
	\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline
		Filter &  \multicolumn{4}{c}{Velocity [\si{\meter\per\second}]} \vline \\
		\hline
		&  $\rmsd( x)$& $\rmsd(y)$ & $\rmsd(z)$ & $\rmsd(||.||)$ \\
		\hline
%		\makecell{\ac{INS} \ac{MEKF} \\ without tether sag compensation} & 1.0036&	0.5577&	0.2858	&1.1832\\
%		\hline
%			\makecell{	\ac{INS} \ac{MEKF} \\ with tether sag compensation} &0.6752	&0.7801&	0.3014&	1.0749	 \\
%		\hline
		\input{MatlabTableExport/VeltetherSagComp2019-05-18_16-07-47.LatexTable}
	\end{tabular}
	\caption{Estimated velocity error with and without tether sag compensation.}
	\label{tab:VeltetherSagComp}
\end{table}
\begin{figure}[ht]
	\centering
	\includegraphics[width=\columnwidth ]{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/TetherSagCompensation/tetherSagCompensation}
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/TetherSagCompensation/withoutTetherSagCompensationPosVel.tex}
	\caption{Position and velocity of the INS MEKF filter without tether sag compensation.}
	\label{fig:AngletetherSagComp}
\end{figure}