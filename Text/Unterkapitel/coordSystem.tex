% !TeX spellcheck = en_US
\section{Coordinate systems}

\subsection{Earth fixed frame}

To determine the position and the attitude of the kite, a fixed frame, which does not move or rotate with the kite is defined. This frame is tangential to the base station and the x-Axis directs to geographic north, the y-Axis to east and the z-Axis downward. It is called \ac{NED} frame\cite{Sedelmaier2018,Barton2012}.

\subsection{Body fixed frame}
The accelerometer, the magnetometer and the gyroscope are mounted on the kite, whereas the measured data changes with the attitude of the kite. For this a new frame which is fixed to the body of the kite is introduced as shown in figure \ref{fig:body_fixed_frame}. The x-Axis is aligned with the nose of the kite and always directs in the direction of flight. The y-Axis shows to the right from the x-Axis, and the z-Axis downward to get a right handside orthogonal system. The rotations are defined as mathematically positive rotations around the corresponding axis. In this definition, a positive angle around the y-Axis named pitch angle means the angle of attack increases.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\columnwidth ]{../Bilder/Yaw_Axis_Corrected}\\
	\small Source: Adapted from \cite{Auawise2019}  
	\caption{Frame definition.}
	\label{fig:body_fixed_frame}
\end{figure}

\subsection{Rotating from one frame to another}
\label{sec:rotation_matrices}
To determine the sensor data in the earth fixed frame or determining the gravity or the magnetic field in the body frame, rotations must be defined. 
\subsubsection{Euler angles}
\label{subsec:euler_angles}
A Common way to determine the attitude of a body in a different frame are the Euler angles. This representation is more representative than others like the quaternions described in section \ref{sec:quaternions}. The drawback is, that in specific angles, like the pitch angle at $\pm \si{90}{\degree}$, the calculations between different frames have singularities. In this cases it is impossible to determine the angles uniquely, also known as gimbal lock\cite{Diebel2006}. A second disadvantage is the need of higher computational power, due to the trigonometric functions. The calculation is slower than calculating with quaternions\cite{Diebel2006}. The Euler angles are defined from the earth fixed frame to the body fixed frame shown in figure \ref{fig:euler_angle_definition}\cite{Diebel2006}.
\FloatBarrier
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\columnwidth ]{../Bilder/EulerAngles}\\
	\small Source: Adapted from \cite{Auawise2019}
	\caption{Definition of the Euler angles.}
	\label{fig:euler_angle_definition}
\end{figure}
\FloatBarrier
Rotating a vector $\boldsymbol{z}$ from the earth fixed frame to the body fixed frame is defined as
\begin{equation}
\boldsymbol{z}_b = \boldsymbol{R}_e^b \boldsymbol{z}_e
\end{equation}
with the rotation matrix
\begin{equation}
\boldsymbol{R}_e^b(\phi , \theta, \psi) = \boldsymbol{R}_x(\phi ) \boldsymbol{R}_y(\theta ) \boldsymbol{R}_z(\psi )
\end{equation}%
\nomenclature{$\phi$}{rotation angle about the $x$ axis (roll)\nomunit{\si{\radian}}}%
\nomenclature{$\theta$}{rotation angle about the $y$ axis (pitch)\nomunit{\si{\radian}}}%
\nomenclature{$\psi$}{rotation angle about the $z$ axis (yaw)\nomunit{\si{\radian}}}%
which is the multiplication of rotation matrices about the individual axis $x$, $y$ and $z$ defined as
\begin{equation}
	\boldsymbol{R}_x(\phi )  = \begin{bmatrix}1 & 0 & 0\\
	0 & \cos{(\phi )} & \sin{(\phi )}\\
	0 & -\sin{(\phi )} & \cos{(\phi )}\end{bmatrix}\\
\end{equation}

\begin{equation}
\boldsymbol{R}_y (\theta) = \begin{bmatrix}\cos{(\theta )} & 0 & -\sin{(\theta )}\\
0 & 1 & 0\\
\sin{( \theta )} & 0 & \cos{(\theta )}\end{bmatrix}
\end{equation}

\begin{equation}
\boldsymbol{R}_z(\psi ) = \begin{bmatrix}\cos{(\psi )} & \sin{(\psi )} & 0\\
-\sin{(\psi )} & \cos{(\psi )} & 0\\
0 & 0 & 1\end{bmatrix}\text{.}
\end{equation}
This definition of the rotation order is also called Cardan angles and commonly used in the aerospace field\cite{Diebel2006}. The inverse rotation from the body fixed frame to the earth fixed frame is
\begin{equation}
\boldsymbol{R}_b^e = \boldsymbol{R}_e^{b^{-1}} = \boldsymbol{R}_e^{b^{T}} \text{.}
\end{equation}
Due to the rotations one after the other about the $z$, $y$ and then about the $x$ axis, the derivative of the Euler angles with respect to the angular velocity in the body frame derived by \cite{Diebel2006} is
\begin{equation}
\label{eq:gimbal_lock}
\begin{bmatrix}
\dot{\phi} \\
\dot{\theta} \\
\dot{\psi}
\end{bmatrix} = \underbrace{\begin{bmatrix} 
	1 & sin(\phi ) tan(\theta ) & cos(\phi ) tan(\theta ) \\
	0 & cos(\theta ) & -sin(\theta) \\
	0 & \frac{sin(\phi ) }{cos(\theta )} & \frac{cos(\phi )}{cos(\theta )}
	\end{bmatrix}}_{\coloneqq S} \boldsymbol{\omega}_b\text{.}
\end{equation}
It is visible, that at angles of $\pm \si{90}{\degree}$ the denominator of the elements of the third row become zero and the matrix becomes singular. This means no unique solution to calculate the angular velocity in the earth frame is available. A possible solution of this problem is to define the frame, so that no gimbal lock occurs, respectively no angle of $\pm \si{90}{\degree}$ for the angle $\theta$ occurs.


\subsubsection{Unit quaternions}
\label{sec:quaternions}
A second representation of a rotation is the unit quaternion. This is a quaternion with a special constraint. First the quaternion operations are presented and then the constraint to create a unit quaternion. A quaternion is presented by a four dimensional vector
\begin{equation}
\label{eq: }
\boldsymbol{q} = \begin{bmatrix}
q_0 \\ q_1 \\q_2 \\q_3
\end{bmatrix}\text{.}
\end{equation}
This vector can be splitted up into a scalar part 
\begin{equation}
\label{eq: }
q_s = q_0
\end{equation}
and a 3 dimensional vector part 
\begin{equation}
\label{eq: }
\boldsymbol{q}_v = \begin{bmatrix}
q_1 \\q_2 \\q_3
\end{bmatrix}\text{.}
\end{equation}
Due to the four dimensional representation, no singularities can occur\cite{Diebel2006}. Another benefit of the quaternion representation is, the calculations of them is more accurate and faster than using Euler angles because of the accuracy and the calculation time of trigonometric functions. No trigonometric functions are used in the quaternion representation\cite{Diebel2006}.
There are several different definitions of the multiplication of quaternions \cite{Maley2013,Markley2003,PhilippeMartin2010} which are all equivalent. In this thesis the definition of J. M. Maley
\begin{equation}
\label{eq:quaternion_multiplication}
\boldsymbol{p} \otimes \boldsymbol{q} = \left. \begin{matrix}
p_s q_s - \boldsymbol{p}_v \cdot  \boldsymbol{q}_v \\
p_s \boldsymbol{q}_v + q_s \boldsymbol{p}_v + \boldsymbol{p}_v \times  \boldsymbol{q}_v
\end{matrix} \right]
\end{equation}
is used \cite{Maley2013}. The norm of a quaternion is defined same as the norm for normal vectors as
\begin{equation}
\label{eq:quaternion_norm}
||\boldsymbol{q}|| = \sqrt{q_s^2 + q_{v_1}^2+ q_{v_2}^2+ q_{v_3}^2}
\end{equation}
and the adjoint operation is defined as
\begin{equation}
\label{eq:quaternion_adjoint}
\bar{\boldsymbol{q}} = \begin{bmatrix}
q_s \\
-\boldsymbol{q}_v
\end{bmatrix}\text{.}
\end{equation}
This function is used in the inverse operation which is defined as the adjoint of the quaternion divided by the norm of a quaternion 
\begin{equation}
\label{eq:quaterion_inv}
\boldsymbol{q}^{-1} = \frac{\bar{\boldsymbol{q}}}{||\boldsymbol{q}||}\text{.}
\end{equation}
A unit quaternion is a quaternion with the constraint that the norm of the quaternion is equal to one. With this constraint, the multiplication of unit quaternions $\boldsymbol{p} \otimes \boldsymbol{p}$ can be illustrated as a rotation of the unit quaternion $\boldsymbol{p}$ about the angle of $\boldsymbol{q}$. The result is again a unit quaternion.
A three dimensional vector $\boldsymbol{\omega} \in \mathbb{R}^3$ can be seen as a quaternion with scalar part zero and as vector part the three dimensional vector\cite{PhilippeMartin2010}
\begin{equation}
\label{eq: }
\boldsymbol{q}_\omega = \begin{bmatrix}
0 \\
\boldsymbol{\omega}
\end{bmatrix}
\end{equation}
A scalar $s \in \mathbb{R} $ can be seen as a quaternion with scalar part $s$ and vectorpart zero
\begin{equation}
\label{eq: }
\boldsymbol{q}_s = \begin{bmatrix}
s \\
0 \\
0 \\
0\\
\end{bmatrix}\text{.}
\end{equation}
Equivalent to the frame representation in the Euler angle representation, a vector $\boldsymbol{\omega}_e \in \mathbb{R}^3$ in the earth frame can be represented in the body frame by multiplying the vector from the left side with the unit quaternion which represents the rotation from the earth frame to the body frame and from the right side with its inverse
\begin{equation}
\label{eq:quaternion_frame_change}
\begin{bmatrix}
0 \\
\boldsymbol{\omega}_b
\end{bmatrix} = \boldsymbol{q} \otimes \begin{bmatrix}
0 \\
\boldsymbol{\omega}_e
\end{bmatrix} \otimes \boldsymbol{q}^{-1}\text{.}
\end{equation}%
\nomenclature{$\boldsymbol{\omega}_b$}{$\in \mathbb{R}^3$, angular rate in the body frame\nomunit{\si{\radian\per\second}}}%
\nomenclature{$\boldsymbol{\omega}_b$}{$\in \mathbb{R}^3$, angular rate in the inertia frame\nomunit{\si{\radian\per\second}}}%
This equation can be rewritten to reduce the number of calculations by omitting the scalar part. The result is
\begin{equation}
\label{eq: }
\boldsymbol{\omega}_b = \boldsymbol{R}_e^b(\boldsymbol{q})\boldsymbol{\omega}_e
\end{equation}
with the quaternion rotation matrix
\begin{equation}
\label{eq:quaterion_matrix_frame_change}
R_e^b(\boldsymbol{q}) = \begin{bmatrix}
q_0^2+q_1^2-q_2^2-q_3^2 & 2q_1 q_2 + 2 q_0 q_3 & 2 q_1 q_3 - 2 q_0 q_2 \\
2 q_1 q_2 - 2 q_0 q_3 & q_0^2 - q_1^2 + q_2^2 - q_3^2 & 2 q_2 q_3 + 2 q_0 q_1 \\
2 q_1 q_3 + 2 q_0 q_2 & 2 q_2 q_3 - 2 q_0 q_1 & q_0^2 - q_1^2 - q_2^2 + q_3^2
\end{bmatrix}\text{.}
\end{equation}
To rotate a vector from the body frame to the earth frame, the matrix \eqref{eq:quaterion_matrix_frame_change} must be inverted, which is equivalent to the transpose of the matrix
\begin{equation}
\label{eq: }
\boldsymbol{\omega}_e = R_e^b(\boldsymbol{q})^T \boldsymbol{\omega}_b\text{.}
\end{equation}
To calculate the Euler angles from a unit quaternion 
\begin{equation}
\label{eq:quaternionToEuler}
\begin{bmatrix}
\phi \\
\theta \\
\psi
\end{bmatrix} = \begin{bmatrix}
atan2(2q_2 q_3 + 2q_0 q_1, q_3^2 - q_2^2 - q_1^2 + q_0^2) \\
-asin(2q_1 q_3 - 2q_0q_2) \\
atan2(2q_1q_2 + 2 q_0 q_3, q_1^2 + q_0^2 - q_3^2 - q_2^2)
\end{bmatrix}
\end{equation}
is used. An important unit quaternion is the identity quaternion
\begin{equation}
\label{eq:quaternionIdentity}
\boldsymbol{q}_1 = \begin{bmatrix}
1 \\
0 \\
0 \\
0
\end{bmatrix}
\end{equation}
which is represented with the scalar part equal to one and the vector part equal to zero. Using \eqref{eq:quaternionToEuler} it can be seen, that the identity quaternion corresponds to the attitude with all angles $\phi , \theta , \psi$ zero
\begin{equation}
\label{eq: }
\begin{bmatrix}
	\phi \\
	\theta \\
	\psi
	\end{bmatrix} = \begin{bmatrix}
	0 \\
	0\\
	0
	\end{bmatrix}\text{.}
\end{equation}
The reverse calculation from Euler angles to quaternions is defined in \cite{Diebel2006} as
\begin{equation}
\label{eq: }
\boldsymbol{q} = 
\begin{bmatrix}
cos(\phi/2)cos(\theta/2)cos(\psi/2)+sin(\phi/2)sin(\theta/2)sin(\psi/2) \\
-cos(\phi/2)sin(\theta/2)sin(\psi/2)+cos(\theta/2)cos(\psi/2)sin(\phi/2) \\
cos(\phi/2)cos(\psi/2)sin(\theta/2)+sin(\phi/2)cos(\theta/2)sin(\psi/2) \\
cos(\phi/2)cos(\theta/2)sin(\psi/2)-sin(\phi/2)cos(\psi/2)sin(\theta/2)\\
\end{bmatrix}\text{.}
\end{equation}
The time derivative of a unit quaternion can be expressed in terms of the unit quaternion itself and the angular velocity in the body frame as
\begin{equation}
\label{eq:quaternion_derivative}
\dot{\boldsymbol{q}} = \frac{1}{2} \boldsymbol{q} \otimes\begin{bmatrix}
0 \\
\boldsymbol{\omega}_b
\end{bmatrix}
\end{equation}
Integrating the discrete angular velocity to retrieve the attitude in the unit quaternion representation can be done by using
\begin{equation}
\label{eq:quaternionIntegration}
\boldsymbol{q}_k = e^{\frac{1}{2} \bar{\boldsymbol{\omega}}_\times \Delta t}\boldsymbol{q}_{k-1} 
\end{equation}%
\nomenclature{$\Delta t$}{sample time\nomunit{\si{\second}}}
with $\bar{\boldsymbol{\omega}}_\times $ the cross product matrix 
\begin{equation}
\label{eq: }
\boldsymbol{\omega}_\times = \begin{bmatrix}
0 & -\omega_3 & \omega_2 \\
\omega_3 & 0 & -\omega_1 \\
-\omega_2 & \omega_1 & 0
\end{bmatrix}
\end{equation}
of the mean of the angular velocity defined as
\begin{equation}
\label{eq: }
\bar{\boldsymbol{\omega}} = \frac{\boldsymbol{\omega}_{b_k} + \boldsymbol{\omega}_{b_{k-1}}}{2}
\end{equation}
where $\boldsymbol{\omega}_{b_k}$ defines the angular velocity at step $k$ and $\boldsymbol{\omega}_{b_{k-1}}$ the angular velocity at step $k-1$.