\section{Sensors}
The \ac{AWES}, which is developed, has multiple sensors on board to estimate the attitude, the position and the velocity of the kite. In this chapter the used sensors are explained.
\subsection{Accelerometer}
\label{sec:accelerometer}
The accelerometer measures the acceleration of the body. In steady state with no movement, the accelerometer measures the gravity 
\begin{equation}
\label{eq:gravity}
\boldsymbol{g}_e = \left. \begin{matrix}
0 \\
0 \\
9.81
\end{matrix} \right]
\end{equation}%
\nomenclature{$\boldsymbol{g}_e$}{$\in \mathbb{R}^3$ the gravity vector in the inertia frame
\nomunit{\si{\meter\per\square\second}}}%
[\si{\meter\per\square\second}], but while moving and rotating the sensor measures two additional accelerations, the translational $\dot{\boldsymbol{v}}_b$ and the centripetal acceleration $\boldsymbol{a}_{cp}$\cite{Barton2012}. The acceleration in body coordinates is defined as
\begin{equation}
\label{eq:acceleration}
\boldsymbol{a}_b = \dot{\boldsymbol{v}}_b + \boldsymbol{a}_{cp} - \boldsymbol{g}_b
\end{equation}%
with the centripetal acceleration
\begin{equation}
\label{eq: }
\boldsymbol{a}_{cp} = \boldsymbol{\omega}_b \times \boldsymbol{v}_b
\end{equation}
\nomenclature{$\boldsymbol{a}_b$}{$\in \mathbb{R}^3$, acceleration in the body coordinates\nomunit{\si{\meter\per\square\second}}}%
\nomenclature{$\boldsymbol{a}_{cp}$}{$\in \mathbb{R}^3$, centripetal acceleration\nomunit{\si{\meter\per\square\second}}}%
\nomenclature{$\dot{\boldsymbol{v}}_b$}{$\in \mathbb{R}^3$, translational acceleration in the body coordinates frame\nomunit{\si{\meter\per\square\second}}}%
\nomenclature{$\boldsymbol{v}_b$}{$\in \mathbb{R}^3$, velocity in the body coordinates\nomunit{\si{\meter\per\second}}}%
\nomenclature{$\boldsymbol{g}_b$}{$\in \mathbb{R}^3$, gravity vector in body coordinates\nomunit{\si{\meter\per\square\second}}}%
and the gravity vector in body coordinates $\boldsymbol{g}_b$. $\boldsymbol{v}_b$ describes the velocity in the body coordinates.
\nomenclature{$\boldsymbol{v}_e$}{$\in \mathbb{R}^3$, velocity in inertia frame\nomunit{\si{\meter\per\second}}}%
The accelerometer is not able to determine all three Euler angles itself, only the roll and pitch angle are determinable. Additional sensors are required to determine the complete attitude.
The measured acceleration is the real acceleration plus an additive noise
\begin{equation}
\label{eq: }
\boldsymbol{a}_{b_{meas}} = \boldsymbol{a}_b + \boldsymbol{\eta}_{a_b}\text{.}
\end{equation}%
\nomenclature{$\boldsymbol{\eta}_{a_b}$}{$\in \mathbb{R}^3$, gaussian noise of the accelerometer in body coordinates\nomunit{\si{\meter\per\square\second}}}
\subsection{Gyroscope}
A gyroscope is a sensor, many times combined in one module with the accelerometer, which measures the angular rate about a specific axis. This means, the measurements are in the body fixed frame. An \ac{IMU} based sensor combines three orthogonal gyroscopes together to get the angular rates for the roll, pitch and yaw axis. The calculation of the Euler angles / quaternions from angular rates are already described in \eqref{eq:gimbal_lock} and \eqref{eq:quaternion_derivative}. Due to gyroscope biases the integration of this values lead to drift in the attitude, whereas absolute angle measurement units like the magnetometer in \ref{sec:magnetometer} and the accelerometer in \ref{sec:accelerometer} are necessary. The gyroscope is used because of its high sample rate to integrate the angles, when any measurements from the accelerometer or magnetometer are available. The gyroscope measurements with gyroscope bias and noise can be described as
\begin{equation}
\label{eq:gyroscope}
\boldsymbol{\omega}_{b_{meas}} = \boldsymbol{\omega}_b + \boldsymbol{b}_w + \boldsymbol{\eta}_{\omega_b}\text{.}
\end{equation}%
with $\boldsymbol{b}_\omega$ the gyroscope bias and $\boldsymbol{\eta}_{\omega_b}$ the additive white gaussian noise
\nomenclature{$\boldsymbol{b}_\omega$}{$\in \mathbb{R}^3$, constant/ slow varying gyroscope bias\nomunit{\si{\radian\per\second}}}%
\nomenclature{$\boldsymbol{\eta}_{\omega_b}$}{white gaussian noise from the gyroscope}
\subsection{Magnetometer}
\label{sec:magnetometer}
One possibility to determine the yaw angle, is a magnetometer, which measures the strength and the direction of the earth magnetic field. If the x component of the magnetometer directs in the direction of the magnetic field, the magnetometer would measure a magnetic field only on the x-Axis
\begin{equation}
\label{eq: }
\boldsymbol{m}_{mag} = \left. \begin{matrix}
B \\
0 \\
0\\
\end{matrix} \right]
\end{equation}%
\nomenclature{$\boldsymbol{m}_{mag}$}{$\in \mathbb{R}^3$, magnetic field in magnetic coordinates in the direction of the magnetic north pole\nomunit{\si{\micro\tesla}}}%
\nomenclature{$B$}{strength of the magnetic field\nomunit{\si{\micro\tesla}}}%
with $B$ the strenght of the field.
The magnetic north is not aligned with the geographic north\cite{inbook,EnvironmentalInformation2019} and therefore, to receive the magnetic field in the \ac{NED} frame the magnetic vector must be rotated by the inclination angle $\delta _i$ about the y-Axis and by the declination angle $\delta _d$ about the z-Axis
\begin{equation}
\label{eq: }
\boldsymbol{m}_e = \boldsymbol{R}_{mag}^e \boldsymbol{m}_{mag} = \boldsymbol{R}_z(\delta _d)\boldsymbol{R}_y(\delta _i) \boldsymbol{m}_{mag}
\end{equation}%
\nomenclature{$\boldsymbol{m}_e$}{$\in \mathbb{R}^3$, magnetic field in inertia coordinates\nomunit{\si{\micro\tesla}}}%
\nomenclature{$\delta_d$}{magnetic declination\nomunit{\si{\degree}}}%
\nomenclature{$\delta_i$}{magnetic inclination\nomunit{\si{\degree}}}%
with the rotation matrices defined in \ref{sec:rotation_matrices}. The inclination angle vary over the earth surface, at south with $-90\degree$ to $+90\degree$ in the north which is illustrated in figure \ref{fig:inclination}. The angles can be determined by positioning the magnetometer in the direction of geographic north on flat ground with pitch and roll angle equal to zero and using the equations
\begin{equation}
\label{eq:inclination}
\delta _i = \frac{180 \operatorname{asin}\left( \mathit{m_{b_z}}\right) }{\ensuremath{\pi} }
\end{equation}
and
\begin{equation}
\label{eq:declination}
\delta _d = \frac{180 \operatorname{acos}\left( \frac{\mathit{m_{b_x}}}{\sqrt{1-{{\mathit{m_{b_z}}}^{2}}}}\right) }{\ensuremath{\pi} }
\end{equation}
or by using world magnetic models \cite{EnvironmentalInformation2019,Magnetic-Declination.com2019}.
% hier fehlt das minus, da in Systemmatrix.wxm mit -delta_i gedreht wird!


\begin{figure}[ht]
	\includegraphics[width=\columnwidth ]{../Bilder/World_Magnetic_Inclination_2010}
	\small Source: \cite{RockMagnetist2010}
	\caption{Magnetic inclination map.}
	\label{fig:inclination}
\end{figure}
\subsubsection{Calibration}
Due to the environment or magnetic distortion through electronic devices the measured direction is not aligned with the magnetic field, which means, the magnetometer must be calibrated to compensate these effects. The magnetic distortion can be classified into two main distortions, the hard-iron and the soft-iron magnetic distortion\cite{Konvalin2008}.
\subsubsection{Hard-iron Calibration}
The hard-iron distortion comes mainly from ferromagnetic components on the \ac{PCB} or other ferromagnetic components like cases or electric motors next to the sensor \cite{Ozyagcilar2015,Ozyagcilar2015a}. These components rotate together with the sensor and therefore this distortion is an additive offset
\begin{equation}
\label{eq: }
\boldsymbol{m}_{b_{meas}} = \boldsymbol{m}_b + \boldsymbol{m}_{b_{HI}}
\end{equation}
with $\boldsymbol{m}_{b_{HI}}$ the hard iron distortion. The offsets and the strength of the magnetic field can be determined by using the previous measured data and the method from Ozyagcilar \cite{Ozyagcilar2015a}. A least square minimization of the residual
\begin{equation}
\label{eq: }
\boldsymbol{r} = \boldsymbol{Y} - \boldsymbol{X}\boldsymbol{\beta}
\end{equation}
with $\boldsymbol{Y}$, $\boldsymbol{X}$ and $\boldsymbol{\beta}$ defined as
\begin{equation}
\label{eq: }
\boldsymbol{y} = \begin{bmatrix}
m_{b_{meas_x}}[0]^2 + m_{b_{meas_y}}[0]^2 + m_{b_{meas_z}}[0]^2 \\
m_{b_{meas_x}}[1]^2 + m_{b_{meas_y}}[1]^2 + m_{b_{meas_z}}[1]^2 \\
\cdots \\
m_{b_{meas_x}}[M-1]^2 + m_{b_{meas_y}}[M-1]^2 + m_{b_{meas_z}}[M-1]^2 \\
\end{bmatrix}
\end{equation}

\begin{equation}
\label{eq: }
\boldsymbol{X} = \begin{bmatrix}
m_{b_{meas_x}}[0] & m_{b_{meas_y}}[0] & m_{b_{meas_z}}[0] & 1 \\
m_{b_{meas_x}}[1] & m_{b_{meas_y}}[1] & m_{b_{meas_z}}[1] & 1 \\
\vdots 		  &		\vdots    &      \vdots   & \vdots \\
m_{b_{meas_x}}[M-1] & m_{b_{meas_y}}[M-1] & m_{b_{meas_z}}[M-1] & 1 \\
\end{bmatrix}
\end{equation}

\begin{equation}
\label{eq:calculateOffset}
\boldsymbol{\beta} = \left[ \begin{matrix}
2 B_{HI_x} \\
2 B_{HI_y} \\
2 B_{HI_z} \\
B^2 - B_{HI_x}^2 - B_{HI_y}^2 - B_{HI_z}^2 
\end{matrix} \right]
\end{equation}
is made. The solution of this minimization problem is
\begin{equation}
\label{eq: }
\boldsymbol{\beta} = (\boldsymbol{X}^T \boldsymbol{X})^{-1}\boldsymbol{X}^T \boldsymbol{y}\text{.}
\end{equation}
After solving this equation, the hard-iron offsets can be calculated with equation \eqref{eq:calculateOffset}.
%\begin{figure}[ht]
%	\centering
%	\input{../Simulationen/KalmanFilterAttitudeEstimation/SimulationResults/MagnetometerCalibration/MagnetometerHardIronCalibration.tex}
%	\caption{Compensating hard iron distortion}
%	\label{fig:HI_compensation}
%\end{figure}

\subsubsection{Soft-iron Calibration}
The soft iron distortion is not anymore a simple offset. The soft iron distortion depends on the environment which is not rotating with the sensor. So this calibration must be done every time the environement around the sensor changed. The task of this calibration is to find the soft-iron distortion matrix $\boldsymbol{W}$ which is defined as
\begin{equation}
\label{eq:SI_disturbation}
\boldsymbol{m}_{b_{meas}} = \boldsymbol{W} \boldsymbol{m}_b + \boldsymbol{m}_{b_{HI}}\text{.}
\end{equation}
The strength of the earth magnetic field does not change when rotating the magnetometer. Therefore, the magnetic field $\boldsymbol{m}_b = \boldsymbol{W}^{-1}(\boldsymbol{m}_{b_{meas}} - \boldsymbol{m}_{b_{HI}})$ must fit
\begin{equation}
\label{eq: }
\left(\boldsymbol{W}^{-1} (\boldsymbol{m}_{b_{meas}}-\boldsymbol{m}_{b_{HI}})\right)^T\boldsymbol{W}^{-1} (\boldsymbol{m}_{b_{meas}}-\boldsymbol{m}_{b_{HI}}) = B^2
\end{equation}
which describes an ellipsoid
\begin{equation}
\label{eq: }
(\boldsymbol{r}-\boldsymbol{r}_0)^T\boldsymbol{A}(\boldsymbol{r}-\boldsymbol{r}_0) = const
\end{equation}
with $\boldsymbol{A} = \left( \boldsymbol{W}^{-1}\right) ^T \boldsymbol{W}^{-1}$, $\boldsymbol{R} = \boldsymbol{m}_{b_{meas}}$ and center $\boldsymbol{r}_0 = \boldsymbol{m}_{b_{HI}}$\cite{Ozyagcilar2015}.
Through ellipsoid fitting the matrix $\boldsymbol{A}$ can be determined\cite{Turner1970,Li2004,Yuri2015}. If there aren't any other constraints to the matrix $\boldsymbol{W}^{-1}$ it is not possible to calculate $\boldsymbol{W}^{-1}$ uniquely, because the description of a sphere is independent of the rotation. To solve this problem T. Ozyagcliar \cite{Ozyagcilar2015} used the constraint, that the matrix $\boldsymbol{W}^{-1}$ is symmectric and so the soft iron matrix calculates to
\begin{equation}
\label{eq: }
\boldsymbol{W}^{-1} = \boldsymbol{A}^{\frac{1}{2}}\text{.}
\end{equation}
Scaling the ellipsoid without using the symmectric constraint can be done by rotating the ellipsoid, to match the magnetometer axis, scaling the ellipsoid by its radii and then rotating back to its original rotation\cite{Thoughts2012}. This can be done easily, because the eigenvectors of the ellipsoid are the axis of the ellipsoid. The soft iron matrix is then calculated as
\begin{equation}
\label{eq: }
\boldsymbol{W}^{-1} = \boldsymbol{R}_{ev}\boldsymbol{S}\boldsymbol{R}_{ev}^T
\end{equation}
with
\begin{equation}
\label{eq: }
\boldsymbol{R}_{ev} = \begin{bmatrix}
\boldsymbol{ev}_1 & \boldsymbol{ev}_2 & \boldsymbol{ev}_3
\end{bmatrix}
\end{equation}
the rotation matrix with the eigenvectors as entries and
\begin{equation}
\label{eq: }
\boldsymbol{S} = \begin{bmatrix}
r_1 & 0 & 0 \\
0 & r_2 & 0 \\
0 & 0 & r_3
\end{bmatrix}
\end{equation}
with $r_x$, $x\in \{1,2,3\}$ the radii of the ellipsoid, which are the inverse of the square roots of the eigenvalues
\begin{equation}
\label{eq: }
r_x = \frac{1}{\sqrt{|\lambda_x|}}
\end{equation}
\FloatBarrier

\subsection{\ac{GPS}}
\ac{GPS} is a global position navigation system developed by the United States using multiple satellites to determine the position and the velocity of the \ac{GPS} receiver. The more \ac{GPS} satellites are in line of sight, the more accurate are the measurements, but at least four are needed\cite{Schmandt2019}. The measured values are in the inertia frame and must be transformed into the \ac{NED}-frame by subtracting the position of the base in inertia coordinates
\begin{equation}
\label{eq: }
\boldsymbol{p}_{GPS} = \boldsymbol{p}_{GPS_{meas}} - \boldsymbol{p}_{base}\text{.}
\end{equation}%
\nomenclature{$\boldsymbol{p}_{GPS}$}{$\in \mathbb{R}^3$, \ac{GPS} position relative to the base station\nomunit{\si{\meter}}}%
\nomenclature{$\boldsymbol{p}_{GPS_{meas}}$}{$\in \mathbb{R}^3$, \ac{GPS} position measured from the \ac{GPS} module\nomunit{\si{\meter}}}%
\nomenclature{$\boldsymbol{v}_{GPS}$}{$\in \mathbb{R}^3$, velocity in inertia coordinates\nomunit{\si{\meter\per\second}}}%
\nomenclature{$\boldsymbol{\eta}_{P_{GPS}}$}{$\in \mathbb{R}^3$, position noise from the \ac{GPS} module\nomunit{\si{\meter\per\second}}}%
\nomenclature{$\boldsymbol{\eta}_{v_{GPS}}$}{$\in \mathbb{R}^3$, velocity noise of the \ac{GPS} module\nomunit{\si{\meter\per\second}}}%
The \ac{GPS} position and velocity are signals with additive Gaussian noise
\begin{equation}
\label{eq: }
\begin{bmatrix}
\boldsymbol{p}_{GPS_{meas}} \\
\boldsymbol{v}_{GPS_{meas}}
\end{bmatrix} = \begin{bmatrix}
\boldsymbol{p}_{GPS} \\
\boldsymbol{v}_{GPS}
\end{bmatrix} + \begin{bmatrix}
\boldsymbol{\eta}_{p_{GPS}} \\
\boldsymbol{\eta}_{v_{GPS}}
\end{bmatrix}\text{.}
\end{equation}

\subsection{Barometer}
The barometer is a sensor to measure the air pressure. This sensor must be placed in a place, where no wind blows to not falsify the measured data. It is used to calculate the height with the international barometric formula
 \begin{equation}
\label{eq:barometricFormul}
h = 44330 \left( 1-\left( \frac{p}{p_0} \right)^{\frac{1}{5.255}}  \right) 
\end{equation}%
\nomenclature{$h$}{absolute height\nomunit{\si{\meter}}}%
\nomenclature{$p$}{air pressure at height h \nomunit{\si{\newton\per\square\meter}}}%
\nomenclature{$p_0$}{air pressure at sea level\nomunit{\si{\newton\per\square\meter}}}%
from \cite{Sensortec2015} with $p_0$ the pressure at sea level and $p$ the measured pressure. The pressure is a signal with additive Gaussian white noise
\begin{equation}
\label{eq: }
p_{meas} = p + \eta_{p}\text{.}
\end{equation}
\nomenclature{$\eta_p$}{white gaussian noise of the measured pressure\nomunit{\si{\newton\per\square\meter}}}
The height measured with the barometer is much more accurate and has a higher sample rate than the height measured with the \ac{GPS} module, but over a longer time the barometer measured height can drift due to variations in the local barometric pressure\cite{Barton2012}. To solve this problem, the barometer can be recalibrated with the height from the \ac{GPS} module.
 


\subsection{Tether angle sensor}
\label{sec:tetherAngleSensor}
\FloatBarrier
The tether angle sensor consists of two potentiometers, which measure the azimuth and the elevation angle of the tether to the \ac{NED}-frame. With the assumption, the tether is a straight line and knowing the tether length, the position of the kite can be calculated as
\begin{equation}
\label{eq:tetherAngleSensorPositionCalculation}
p_{tether} = r\begin{bmatrix}
sin(\theta_{base}) sin(\phi_{base}) \\
sin(\theta_{base}) cos(\phi_{base}) \\
cos(\theta)_{base}
\end{bmatrix}
\end{equation}%
\nomenclature{$p_{tether}$}{position determined from the tether angle sensor\nomunit{\si{\meter}}}%
\nomenclature{$\theta_{base}$}{azimut angle measured with the tether angle sensor in the base station\nomunit{\si{\radian}}}%
\nomenclature{$\phi_{base}$}{elevation angle measured with the tether angle sensor in the base station\nomunit{\si{\radian}}}%
\nomenclature{$r$}{tether length\nomunit{\si{\meter}}}%
\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\columnwidth ]{../Bilder/3D_Spherical}\\
	\small Source: \cite{Andeggs2009}
	\caption{Calculation of the position of the kite with the tether angle sensor.}
	\label{fig:sphereCoords}
\end{figure}\\
with $\theta_{base}$ the polar angle, $\phi_{base}$ the azimut angle and $r$ the tether length. Due to the mass of the tether the assumption of a straight line is not correct. This and a more correct model is discussed in section \ref{sec:tetherSag}. The measured angles and tether length are distorted by additive Gaussian noise
\begin{equation}
\label{eq: }
r_{meas} = r + \eta_r
\end{equation}
\begin{equation}
\label{eq: }
\theta_{base_{meas}} = \theta_{base} + \eta_{\theta_{base}}
\end{equation}
\begin{equation}
\label{eq: }
\phi_{base_{meas}} = \phi_{base} + \eta_{\phi_{base}}
\end{equation}
\nomenclature{$\eta_r$}{gaussian white noise of the tether length measurement\nomunit{\si{\meter}}}%
\nomenclature{$\eta_{\theta_{base}}$}{gaussian white noise of the azimut angle measurement in the base station\nomunit{\si{\radian}}}%
\nomenclature{$\eta_{\phi_{base}}$}{gaussian white noise of the elevation angle measurement in the base station\nomunit{\si{\radian}}}%
\nomenclature{$\eta_{\theta_{kite}}$}{gaussian white noise of the azimut angle measurement in the kite\nomunit{\si{\radian}}}%
\nomenclature{$\eta_{\phi_{kite}}$}{gaussian white noise of the elevation angle measurement in the kite\nomunit{\si{\radian}}}%
% Bild zeigen wie das ausschaut
\begin{figure}[ht]
	\includegraphics[width=\columnwidth ]{../Bilder/LineAngleSensor2}
	\caption{Tether angle sensor with tether.}
	\label{fig:lineAngleSensor}
\end{figure}
A second tether angle sensor is mounted on the kite and measures the angle of the tether coming into the kite. One reason of this sensor is described in section \ref{sec:tetherSag} where a model for tether sagging is proposed.
\FloatBarrier